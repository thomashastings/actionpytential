import numpy as np

def action_potential_parameters(timeaxis, voltaxis, fdv,
                calculation_settings:{'delaytime': 2.5, 'endtime': 0.0, 'filter_cutoff': 40.0,
                                      'filter_order': 5, 'filter_use': False, 'time_unit': 'ms',
                                      'volt_unit': 'mV', 'voltage_clamp': False, 'slow_response': False, 'rp_mode': 'end'}):
    param_temp={}
    custom_apd=None
    repol_dict={'APD10': 0.9,'APD25': 0.75,'APD50': 0.5,
                'APD75': 0.25,'APD90': 0.1,  'APD95': 0.05,'APDC': 0}

    if custom_apd != None:
        repol_dict['APDC']=custom_apd
    
    ## Finding limits of length
    if len(timeaxis) > len(voltaxis):
        max_length = len(voltaxis)-1
    else:
        max_length = len(timeaxis)-1
    
    ## Checking for changing sampling rates
    time_step = 0
    time_step_fast = 100
    time_step_slow = 0
    sampling_shift = False
    for line in range(1,len(timeaxis)-1):
        time_step = abs(timeaxis[line]-timeaxis[line+1])
        if time_step < time_step_fast:
            time_step_fast = time_step
            
        if time_step > time_step_slow:
            time_step_slow = time_step
    
    if time_step_slow > time_step_fast:
        sampling_shift = True
        time_step = time_step_slow
    
    try:        
        if calculation_settings['slow_response'] == True:
            ### Slow-response parameters --> overlays probably needed on figures (can be done as scatter plot)
            # These parameters are specific to the pacemaking tissues, inlcuding the sinoatrial and AV nodes.
            # Maximum diastolic potential (MDP): most negative potential for each AP
            # Take-off potential (TOP): voltage at measured at the threshold of 0.5 mV/ms dV/dt
            # Slope of early diastolic depolarization (EDD)
            # APD (time between TOP and the following MDP)
            # Parameters based on the definitions from Bucchi et al., 2007
            
            # Generating new first derivative, as it is excluded from filtering for other AP types
            dv_dt_list = []
            for line in range(0, max_length):
                dv = voltaxis[line] - voltaxis[line-1]
                dt = timeaxis[line] - timeaxis[line-1]
                try:
                    dv_dt = dv/dt
                except ZeroDivisionError:
                    dv_dt = 0
                dv_dt_list.append(dv_dt)
            
            # APs are segmented, each segment spans from MDP to MDP
            # Therefore, the first point of the AP can be considered as the MDP
            MDP = param_temp['MDP'] = voltaxis[0]
            
            # Finding TOP based on first derivative
            for line in range(0, max_length):
                if dv_dt_list[line] >= 0.5:
                    param_temp['TOPT'] = timeaxis[line]
                    param_temp['TOPV'] = voltaxis[line]
                    break
                
            # The last line is the MDP following the AP
            param_temp['APD'] = timeaxis[max_length] - param_temp['TOPT']
                    
            # Slope of EDD only approximated (no fitting)
            param_temp['EDD'] = 1000 * (param_temp['TOPV'] - voltaxis[0]) / (param_temp['TOPT'] - timeaxis[0])
   

        ## Defining timeframe
        line=0
        while True:
            if timeaxis[line] > calculation_settings['delaytime']:
                firstline=line
                break
            line=line+1
        lastline=max_length

        if calculation_settings['endtime'] != 0:
            testline=lastline-1
            while True:
                if timeaxis[testline] > calculation_settings['endtime']:
                    testline=testline-1
                else:
                    lastline=testline
                    break
        
        ### Finding RMP
        # The mean diastolic potential or resting membrane potential is the
        #      baseline from which the action potential rises, it can be defined
        #      at the beginning and at the end of the potential.
        rp_mode = calculation_settings['rp_mode']
        if rp_mode == 'end' or rp_mode == 'aver':
            sum_mdp=0
            counter=0
            line = lastline
            while timeaxis[line] > timeaxis[lastline]-20: # 20 ms = 1/50 sec = 1 cycle at 50 Hz grid freq
                sum_mdp = sum_mdp + voltaxis[line]
                line = line-1
                counter=counter+1
            emdp = sum_mdp/counter
            param_temp['RMP']=emdp
            
        if rp_mode == 'begin' or rp_mode == 'aver':
            bmdp = voltaxis[firstline]
            param_temp['RMP']=bmdp
            
        if rp_mode == 'aver':
            amdp=(emdp+bmdp)/2
            param_temp['RMP']=amdp
        
        if rp_mode == 'sr':
            param_temp['RMP']=param_temp['MDP']
        
        mdp=param_temp['RMP']
        
        ### Finding AMP and start of repolarization
        # The amplitude is the difference between the highest point
        #     (end of phase 0) and the RMP 
        maxvolt=-100
        for line in range(firstline,lastline):
            if voltaxis[line] > maxvolt:
                maxvolt = float(voltaxis[line+1])
                maxvolt_line=line+1
        
        amp = maxvolt-mdp
        param_temp['AMP'] = amp
        param_temp['maxvolt_line']=maxvolt_line

        ### Finding CT, dVmax, and dVmin
        # Conduction time is the time from 0 ms to to the midpoint of phase 0,
        #     mostly depends on the velocity sodium channel activation.
        # dVmax is the maximal rate of depolarization, also governed by sodium channels.
        line = maxvolt_line
        dvmax = 0
        dvmin = 0
        dvmin_time = 0
        dvmax_line = 0
        ct_value = 0
        time_step = 0
        
        while True:
            if voltaxis[line] > (mdp + amp/2):
                ct_value = timeaxis[line-1]
            
            linevolt = voltaxis[line]
            
            if timeaxis[line] < calculation_settings['delaytime']:
                break
            
            if linevolt > mdp:
                if fdv[line] > dvmax:
                    dvmax = fdv[line]
                    dvmax_time = timeaxis[line]
                    dvmax_line = line
                    
            elif float(voltaxis[line]) < mdp:
                break
            
            elif line < firstline+1:
                break
            
            line = line-1 # going backwards from the top of the potential
        
        line = maxvolt_line
        
        while True:
            if abs(timeaxis[line]-timeaxis[line+1]) > time_step:
                time_step = abs(timeaxis[line]-timeaxis[line+1])
                dvmin = 0 # dual acquisition rate masks late repolarization dVmin
                          # should be taken from the end of the potential actually,
                          # with a cutoff at ~APD25-50 to keep phases 1 and 2-3 separated
            
            if fdv[line] < dvmin:
                dvmin = fdv[line]
                dvmin_time = timeaxis[line]
            
            elif float(voltaxis[line]) < mdp + 2:
                break
            line = line+1 # going forward from the top of the potential
        
        param_temp['CT'] = ct_value
        param_temp['dvmax'] = dvmax
        param_temp['dvmax_time'] = dvmax_time = timeaxis[dvmax_line]
        param_temp['dvmin'] = dvmin
        param_temp['dvmin_time'] = dvmin_time
        
        # Voltage clamp: stimulation artefacts break phase 0, possibly
        #      resulting in unrealistically high AMP and dVmax values.
        #      However, this mostly happens under 0 mV, therefore, going from
        #      phase 3 towards phase 0, finding the amplitude could also be
        #      possible by finding the highest  point before we go ounder 0 mV
        #      (the beginning of overshoot).
        if calculation_settings['voltage_clamp'] == True and rp_mode == 'end' and maxvolt > 0: # only viable if "end" rp_mode is used
            dvmax = 0
            dvmin = 0
            dvmin_time = 0
            dvmax_line = 0
            ct_value = 0
            maxvolt = -100
            line = lastline
            overshoot_ended = False
            overshoot_began = False
            #print(timeaxis[line])
            while True:
                if voltaxis[line] > 0:
                    if not overshoot_ended:
                        overshoot_ended = True
                    else: # going backwards, this is around APD50-75
                        if voltaxis[line] > maxvolt:
                            maxvolt = float(voltaxis[line])
                            maxvolt_line = line
                            amp = maxvolt-mdp
                        if fdv[line] > dvmax:
                            dvmax = fdv[line]
                            dvmax_time = timeaxis[line]
                            dvmax_line = line
                            ct_value = dvmax_time # or AMP/2?
                
                if overshoot_ended and not overshoot_began:
                    if voltaxis[line] < -1: # slight dips below 0 may happen during repolarization
                        overshoot_began = True
                if overshoot_began and overshoot_ended:
                    if fdv[line] < 5 or voltaxis[line] < 0: # upstroke ended
                        break
                line = line-1
        
            # Using new values if they were set
            if amp != 0:
                param_temp['AMP'] = amp
            if maxvolt_line != 0:
                param_temp['maxvolt_line']=maxvolt_line
            if ct_value != 0:
                param_temp['CT'] = ct_value
            if dvmax != 0:
                param_temp['dvmax'] = dvmax
                
        else:
            overshoot_time = 0
            overshoot_voltage = 0
        
        #param_temp['dvmin'] = dvmin
        #param_temp['dvmin_time'] = dvmin_time
        
        # After calcium current activation, the plateau may reach higher points
        #     than the actual amplitude.
        # Therefore, we need to make sure that the point of dVmax and the point
        #      of the amplitude are reasonably close together, in this case,
        #      no more than 10 ms apart. That gives us a minimum dVmax of 14 V/s
        #      for a 140 mV amplitude for correct recognition.
        # Since this is quite rare, detecting this after the fact and rerunning
        #     the AMP-finding part is computationally cheaper, because otherwise
        #     we'd need to iterate through the whole AP more times to find AMP,
        #     dVmax, and CT individually.        
        if timeaxis[maxvolt_line] - dvmax_time > 10 and calculation_settings['slow_response'] == False:
            maxvolt=-100
            line = dvmax_line
            while True:
                if voltaxis[line] > maxvolt:
                    maxvolt = float(voltaxis[line+1])
                    maxvolt_line=line+1
                line = line+1
                if timeaxis[line] > dvmax_time + 10:
                    break
       
            amp = maxvolt-mdp
            param_temp['AMP'] = amp
            param_temp['maxvolt_line'] = maxvolt_line
            
        # APD_temp stores found APD values for the current measurement.
        # APD_f stores the state of APD values being found or not.
        
        APD_f={'APD10': 0,'APD25': 0,'APD50': 0,
                    'APD75': 0,'APD90': 0, 'APD95': 0, 'APDC': 0}
        
        for line in range(lastline, maxvolt_line, -1):
            current_volt=voltaxis[line]-mdp
            for APD in ('APD10','APD25','APD50','APD75','APD90', 'APD95'):
                if APD_f[APD] == 0:
                    if current_volt > amp*repol_dict[APD]:
                        param_temp[APD]=timeaxis[line]-ct_value
                        APD_f[APD]=1
        

        ### Notch-related parameters
        # Strong phase 1 repolarization is characterized by a "notch", a sudden
        #     repolarization followed by depolarization
        # Notch dVmax: not necessarily related to the notch, this is highest
        #      rate of phase 1 repolarization
        # Notch time: the time of the lowest point of the notch
        # Notch voltage: voltage at notch time
        # Notch voltage %: voltage expressed as AMP% at notch time
        
        if APD_f['APD90'] == 1 and APD_f['APD25'] == 1:
            try:
                ## Notch based on derivative of fitted curve                      
                x_fit = []
                y_fit = []
                # Checking for changing sampling rates
                time_step = 0
                time_step_fast = 100
                time_step_slow = 0
                time_step_out = abs(timeaxis[3]-timeaxis[4])
                for line in range(len(timeaxis)-1):
                    time_step = abs(timeaxis[line]-timeaxis[line+1])
                    if time_step < time_step_fast:
                        time_step_fast = time_step
                        
                    if time_step > time_step_slow:
                        time_step_slow = time_step
                        
                    if time_step_slow > time_step_fast:
                        time_step_out = time_step_slow
                        
                #log(time_step_out)
                # Generating datalist to fit
                for i in range(maxvolt_line,lastline):
                    if  timeaxis[i] > timeaxis[maxvolt_line] + 1.5 and timeaxis[i] % time_step_out == 0:
                        x_fit.append(timeaxis[i])
                        y_fit.append(voltaxis[i])
                    if timeaxis[i] > param_temp['APD90']/3:
                        if x_fit != []:
                            break
                # Finding best degree model
                models = []
                rsq_old = 0
                final_rsq_num = 0
                for degree in range(1,30):
                    coeffs = np.polyfit(x_fit, y_fit, degree)
                    p = np.poly1d(coeffs)
                    yhat = p(x_fit)
                    ybar = np.sum(y_fit)/len(y_fit)
                    ssreg = np.sum((yhat-ybar)**2)
                    sstot = np.sum((y_fit - ybar)**2)
                    rsq = 1-(((1-(ssreg/sstot))*(len(y_fit)-1))/(len(y_fit)-degree-1))
                    if rsq > rsq_old:
                        rsq_old = rsq
                        final_rsq = rsq
                        final_rsq_num = degree
                        
                # Creating model
                y_model = np.poly1d(np.polyfit(x_fit, y_fit, final_rsq_num))
                
                dv_dt_list_pos = []
                x_dv_dt_pos = []
                dv_dt_list_neg = []
                x_dv_dt_neg = []
                for line in range(len(x_fit)):
                    dv = y_model(x_fit[line]) - y_model(x_fit[line-1])
                    dt = x_fit[line] - x_fit[line-1]
                    try:
                        dv_dt = dv/dt
                    except ZeroDivisionError:
                        dv_dt = 0
                    if dv_dt > 0.025:
                        dv_dt_list_pos.append(dv_dt*10)
                        x_dv_dt_pos.append(x_fit[line])
                    else:
                        dv_dt_list_neg.append(dv_dt*10)
                        x_dv_dt_neg.append(x_fit[line])
                
                if x_dv_dt_pos != [] and x_dv_dt_pos[len(x_dv_dt_pos)-1] - x_dv_dt_pos[0] >= 1:
                    notch_found = True
                    notch_time = x_dv_dt_pos[0]
                    for line in range(maxvolt_line, lastline):
                        if timeaxis[line] > notch_time:
                            notch_start_line = timeaxis[line-1]
                            notch_line = line
                            break
                else:
                    notch_found = False
                    
                #fitted_timeaxis = [] # this could be used to display the fitted portion of the potential
                #fitted_timeaxis.append(x_fit)
                #fitted_voltaxis = []
                #fitted_voltaxis.append(y_model(x_fit))
            
                if notch_found == True:
                    notch_volt_min = voltaxis[maxvolt_line]
                    for line in range(notch_line, maxvolt_line, -1):
                        if voltaxis[line] <= notch_volt_min:
                            notch_volt_min = voltaxis[line]
                            notch_volt_min_line_left = line
                    notch_volt_max_line = None
                    if 'notch_volt_min_line_left' in vars():
                        for line in range(notch_volt_min_line_left,lastline):
                            if voltaxis[line] > notch_volt_min:
                                notch_volt_max = voltaxis[line] # does this represent the plateau voltage in notched potentials?
                                notch_volt_max_line = line
                        if notch_volt_max_line == None:
                            notch_volt_max_line = notch_volt_min_line_right = notch_volt_min_line_left
                        else:
                            for line in range(notch_volt_min_line_left, notch_volt_max_line):
                                if voltaxis[line] <= notch_volt_min:
                                    notch_volt_min_line_right = line
                        for line in range(notch_volt_min_line_left-2,notch_volt_max_line): # backwards, in case of isoelectric region
                            if voltaxis[line] < notch_volt_min:
                                notch_volt_min_line_right = line
                        notch_line = round((notch_volt_min_line_left + notch_volt_min_line_right)/2)
                        notch_volt_min = voltaxis[notch_line]
                        notch_time = timeaxis[notch_line]
                        notch_relative_volt=voltaxis[maxvolt_line]-notch_volt_min
                        notch_relative_volt_percent = 100*round(notch_relative_volt/param_temp['AMP'],3)
                        param_temp['NV']=round(notch_volt_min,1)
                        param_temp['NT']=round(notch_time-param_temp['CT'],1)
                        param_temp['NV-R']=round(notch_relative_volt,1)
                        param_temp['NV-R%']=notch_relative_volt_percent
                    else:
                        param_temp['NV']="NA"
                        param_temp['NT']="NA"
                        param_temp['NV-R']="NA"
                        param_temp['NV-R%']="NA"
                        
                else:
                    param_temp['NV']="NA"
                    param_temp['NT']="NA"
                    param_temp['NV-R']="NA"
                    param_temp['NV-R%']="NA"
            
            except UnicodeDecodeError:
                param_temp['NV']="NA"
                param_temp['NT']="NA"
                param_temp['NV-R']="NA"
                param_temp['NV-R%']="NA"
            
            ## Early repolarization dVmin
            n_dvmin=0
            for line in range(maxvolt_line+1, lastline):
                n_dvmin_temp = fdv[line]
                if n_dvmin_temp < n_dvmin:
                    n_dvmin = n_dvmin_temp
                if timeaxis[line] > param_temp['APD90']/2.5:
                    break
            param_temp['NdVmin']=round(n_dvmin,1)

            ### Plateau-related parameters
            # Plateau time: midpoint of the action potential (=APD90/2, not included in tables)
            # Plateau potential: absolute voltage at plateau time
            # Plateau potential %: voltage expressed as AMP% at pleateau time
            # Mid plateau slope: the voltage difference between two semi-arbitrary points
            #      + and - from the plateau time. Here 50 ms is used when possible.
            # Overshoot: highest voltage reached from 0 mV
            # Overshoot time: time spent above 0 mV
            
            plateau_f={'mps_high': 0,'mps_low':0}
            plateau_time20=param_temp['CT']+param_temp['APD90']*0.20
            plateau_time50=param_temp['CT']+param_temp['APD90']*0.5
            plateau_time75=param_temp['CT']+param_temp['APD90']*0.75
            plateau_time90=param_temp['CT']+param_temp['APD90']*0.9
            param_temp['PV20'] = param_temp['PV50'] = param_temp['PV75'] = param_temp['PV90'] = 0
            
            if param_temp['APD90'] > 110: 
                mps_high_time = plateau_time50 - 50
                mps_low_time = plateau_time50 + 50
                
                for line in range(maxvolt_line, lastline):
                    time = timeaxis[line]
                    if time > mps_high_time:
                        if plateau_f['mps_high'] == 0:
                            mps_high = voltaxis[line]
                            plateau_f['mps_high'] = 1
                
                    if time > mps_low_time:
                        if plateau_f['mps_low'] == 0:
                            mps_low = voltaxis[line]
                            plateau_f['mps_low'] = 1
                
                param_temp['MPS']= mps_high-mps_low
            else:
                param_temp['MPS'] = 'NA'
            
            for line in range(maxvolt_line, lastline):
                time = timeaxis[line]
                if time < plateau_time20:
                    param_temp['PV20'] = voltaxis[line-1]
                    
                if time < plateau_time50:
                    param_temp['PV50'] = voltaxis[line-1]
                
                if time < plateau_time75:
                    param_temp['PV75'] = voltaxis[line-1]
                
                if time < plateau_time90:
                    param_temp['PV90'] = voltaxis[line-1]
        
            param_temp['PV20%'] = 100*((param_temp['PV20']-mdp)/param_temp['AMP'])
            param_temp['PV50%'] = 100*((param_temp['PV50']-mdp)/param_temp['AMP'])
            param_temp['PV75%'] = 100*((param_temp['PV75']-mdp)/param_temp['AMP'])
            param_temp['PV90%'] = 100*((param_temp['PV90']-mdp)/param_temp['AMP'])
            
            ### Triangulation
            param_temp['TR'] = param_temp['APD90']-param_temp['APD25']
            param_temp['lastline'] = lastline
        
        
        overshoot = False
        overshoot_voltage = 0
        overshoot_time = 0
        for line in range(firstline, lastline):
            if voltaxis[line] > 0:
                overshoot = True
            if overshoot == True:
                if voltaxis[line] > overshoot_voltage:
                    overshoot_voltage = voltaxis[line]
                if voltaxis[line] < 0:
                    overshoot_time = timeaxis[line] - param_temp['CT']
                    break
        param_temp['OST'] = overshoot_time
        param_temp['OSV'] = overshoot_voltage      
        
        return param_temp
    
    except:
        # Simplified finding of CT and dVmax
        line = len(timeaxis)-1
        dvmax = 0
        ct_value = 0
        while True:
            dvmax_temp = fdv[line]
            linevolt = voltaxis[line]
            if timeaxis[line] < calculation_settings['delaytime']:
                break
            if dvmax_temp > dvmax:
                dvmax = dvmax_temp
                ct_value = timeaxis[line]
            elif line < 2:
                break
            line = line-1
        
        return {'RMP': 0,'AMP': 0,'CT': ct_value , 'dvmax': dvmax,'dvmin': 0, 'APD10': 0,
                'APD25': 0, 'APD50': 0, 'APD75': 0, 'APD90': 0, 'NdVmin': 0,
                'NV': 0, 'NT': 0, 'NV-R': 0, 'NV-R%': 0, 'MPS': 0, 'PV50': 0,
                'PV50%': 0, 'TR': 0, 'lastline': 0, 'dvmin_time':0, 'maxvolt_line': 0, 'OSV': 0, 'OST':0}

