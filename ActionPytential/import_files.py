import csv
import os
import re

try:
    from scipy import signal
    from scipy.signal import butter
except ModuleNotFoundError:
    scipy_missing = True # this might be the case on some ARM devices,
                         # filtering is skipped.

def check_file(input_file):
    # Trying common delimiter types
    for file_delimiter in (';', ',' ,' ', '\t'):
        found_lines = 0
        try:
            list_pot=[]
            list_pot_old = list(csv.reader(open(input_file, 'r'), delimiter=str(file_delimiter)))
            replaced=0
            for line in range(0,20):
                newline=[]
                try:
                    for element in range(0,len(list_pot_old[10])):
                        oldvalue=(list_pot_old[line][element])
                        newvalue=oldvalue.replace(",",".") # if there is a comma, it will be replaced by a period
                        newline.append(newvalue)
                        if newvalue == oldvalue:
                            pass
                        else:
                            replaced = replaced+1
                except IndexError:
                    pass
                list_pot.append(newline)
            
            found_lines = 0
            for line in range(1,len(list_pot)):
                if line == len(list_pot)-1 and found_lines > 10:
                    # print(input_file, "identified as '", file_delimiter, "' delimited file.")
                    if (replaced/len(list_pot_old[10])) > 2: # if many replacements were made, the file probably has decimal commas
                        replace=True
                    else:
                        replace=False
                    return file_delimiter, replace  
                try:
                    float(list_pot[line][0])
                    found_lines = found_lines+1
                except ValueError:
                    pass
                except IndexError:
                    pass
        except UnicodeDecodeError:
            return False, False
        except IsADirectoryError:
            return False, False
        except FileNotFoundError:
            return False, False
        except:
            return False, False
        
        
def read_potentials(input_file, file_delimiter, decimal_comma, db_file, skip_separation,
                    calculation_settings):
    input_file_path = input_file['filedir'] + '/' + input_file['filename']
    input_file = input_file['filename']    
    # reading potentials from acquisition file
    additional_info = {}
    additional_info['MULTIBEAT'] = False
    firstline = 1 # ignoring 0th line as header
    list_pot_old = list(csv.reader(open(input_file_path, 'r'), delimiter=file_delimiter))
    list_pot=[]
    if decimal_comma == True:
        for line in range(0,len(list_pot_old)-1):
            newline=[]
            for element in range(0,len(list_pot_old[10])):
                try:
                    oldvalue=(list_pot_old[line][element])
                    newvalue=oldvalue.replace(",",".")
                    newline.append(newvalue)
                except IndexError:
                    pass
            list_pot.append(newline)
    else:
        list_pot = list(csv.reader(open(input_file_path, 'r'), delimiter=str(file_delimiter)))

    # Getting APES-type ID from database file
    file = open(input_file_path, 'r')
    acq_re = re.compile('[0-9]+[A-Za-z]+.\S[0-9]+', re.IGNORECASE) # This will match APES-specific headers with identifiers
    acq_id = acq_re.findall(str(list(csv.reader(file, delimiter = str(file_delimiter)))))
    if acq_id != []:
        try:
            acq_date = (str(acq_id[0][:8])).upper()
            try:
                num_string = (str(acq_id[0][-5:])).split('.')[1]
            except IndexError:
                num_string = (str(acq_id[0][-5:])).split('_')[1]
            for i in range(0,len(num_string)):
                try:
                    int(num_string[i:])
                    acq_num = int(num_string[i:])
                    break
                except:
                    pass
            acq_id = acq_date + '.' + str(acq_num)
        except:
            acq_id = "NA"
            
    else:
        # If no APES ID is found, ID is generated from the name of the file
        acq_id = ((str(input_file)).split('.')[0]) 
        for splitter in ('.', '-', '_'):
            if len(acq_id.split(splitter)) > 1 :
                acq_date = acq_id.split(splitter)[0]
                num_string = acq_id.split(splitter)[1]
                for i in range(0,len(num_string)):
                    try:
                        int(num_string[i:])
                        acq_num = int(num_string[i:])
                        break
                    except:
                        pass
        try:
            acq_id = acq_date.upper() + '.' + str(int(acq_num))
        except UnboundLocalError:
            acq_id = input_file # in case the file contains no numbers at all



    # Measurement ID's from csv to array
    if os.path.isfile(db_file):
        list_db = list(csv.reader(open(db_file, 'r')))
        numline = len(list_db)
        # Finding treatment data based on ID
        match = 0
        try:
            str((list_db[1][0])).upper()+ '.' + str(int((list_db[1][1])))
        except IndexError:
            try:
                list_db = list(csv.reader(open(db_file, 'r'),delimiter=';'))
            except IndexError:
                match = 'F'

        for i in range(0,numline):
            try:
                check_id=str((list_db[i][0])).upper()+ '.' + str(int((list_db[i][1])))
                if match ==  0 :
                    if check_id == acq_id:
                        match = 1
                        acq_type=str((list_db[i][2]))
                        print(acq_id, 'identified as', acq_type)
                        break
            except IndexError:
                match = 'F'
                break

        if match == 'F':
            print('Database file not formatted correctly.')
            acq_type=input_file
        if match == 0:
            #print(input_file, 'was not identified, filename is used.')
            acq_type=input_file
        additional_info['ID']=acq_id
        additional_info['MEAS']=acq_type
        
    else:
        #print('No database was provided.')
        acq_type=input_file
        additional_info['ID']=acq_id
        additional_info['MEAS']=acq_type    
    
    # Calculating unit offsets
    if calculation_settings['time_unit'] == 'ms': time_offset = 1
    elif calculation_settings['time_unit'] == 'us': time_offset = 0.001
    elif calculation_settings['time_unit'] == 's': time_offset = 1000
          
    if calculation_settings['volt_unit'] == 'mV': volt_offset = 1
    elif calculation_settings['volt_unit'] == 'pV': volt_offset = 1.E-9
    elif calculation_settings['volt_unit'] == 'nV': volt_offset = 0.000001
    elif calculation_settings['volt_unit'] == 'uV': volt_offset = 0.001
    elif calculation_settings['volt_unit'] == 'V': volt_offset = 1000
    
    # Correcting timeframe
    calculation_settings['delaytime'] = calculation_settings['delaytime'] * time_offset
    calculation_settings['endtime'] = calculation_settings['endtime'] * time_offset
    
    # Finding the beginning of the potential
    cycle_line = None
    potential_started = False
    for line in range(firstline, len(list_pot)-1):
        try:
            float(list_pot[line][0])
            potential_started=True
        except IndexError:
            if potential_started is False:
                firstline=line+1
        except ValueError:
            if str(list_pot[line][0]) == "Cycle": # APES
                cycle_line = line
            if potential_started is False:
                firstline=line+1
    
    # APES protocol name
    try:
        if str(list_pot_old[1][1])[-4:] == ".TRA" or str(list_pot_old[1][1])[-4:] == ".tra":
            additional_info['TRAIN'] = str(list_pot_old[1][1])[:len(list_pot_old[1][1])-4]
        else: additional_info['TRAIN'] = "NA"
    except:
        additional_info['TRAIN'] = "NA"
    
    # Voltage columns
    pot_columns = []
    current_column = 1
    cycle_lengths = []
    while True:
        try:
            float(list_pot[firstline][current_column])
            if cycle_line != None:
                cycle_lengths.append(list_pot[cycle_line][current_column])
            else:
                cycle_lengths.append(0)
            pot_columns.append(current_column)
            current_column = current_column+1
        except ValueError:
            current_column = current_column+1
        except IndexError:
            break

    out_potentials = []
    out_unseparated = []        
    
    for column in range(len(pot_columns)):
        this_voltage_column = []
        time = []
        this_potential = []
        for line in range(firstline,len(list_pot)-1):
            try:
                this_voltage_column.append(float(list_pot[line][pot_columns[column]])*volt_offset)
                time.append(float(list_pot[line][0])*time_offset)
            except ValueError:
                pass
            except IndexError:
                pass        
                
        # unseparated output
        this_unseparated = []
        this_unseparated.append(time)        
        
        # First derivative
        dv_dt_list = []
        firstline = 0
        dv_max = 0
        dv_min = 0
        volt_min = 0
        volt_max = 0
        upstroke_found = False
        upstroke_times = []
        upstroke_lines = []
        rough_end_times = []
        end_times = []
        dv_min_list = []
        dv_min_times_list = []
        after_times = []
        
        for line in range(firstline, len(time)):
            dv = this_voltage_column[line] - this_voltage_column[line-1]
            dt = time[line] - time[line-1]
            try:
                dv_dt = dv/dt
            except ZeroDivisionError:
                dv_dt = 0
            dv_dt_list.append(dv_dt)
            if dv_dt > dv_max:
                dv_max = dv_dt
            if this_voltage_column[line] > volt_max:
                volt_max = this_voltage_column[line]
            if this_voltage_column[line] < volt_min:
                volt_min = this_voltage_column[line]    
        
        # Applying Butterworth filter
        if calculation_settings['filter_use'] == True:
            
            # Getting lowest sampling rate from time axis
            time_step = 0
            for line in range(1, len(time)):
                if abs(time[line-1]-time[line]) > time_step:
                    time_step = abs(time[line-1]-time[line])
            
            # Converting milliseconds to Hz
            sampling_rate = time_step * 1000
  
            # Applying Butterworth filter
            normal_cutoff = calculation_settings['filter_cutoff'] / (0.5 * sampling_rate)
            try:
                b, a = signal.butter(calculation_settings['filter_order'], normal_cutoff, btype='low', analog=False)
                filtered_voltage_column = signal.filtfilt(b, a, this_voltage_column)
                this_voltage_column = filtered_voltage_column
            except ValueError:
                print('Filtering was not possible. Please, check the units of time measurement.')
        this_unseparated.append(this_voltage_column)
        this_unseparated.append(dv_dt_list)
        out_unseparated.append(this_unseparated)

        lengths = [len(time), len(this_voltage_column), len(dv_dt_list)]
        lengths.sort()
        max_length = lengths[0]
        old_time = time
        old_voltage = this_voltage_column
        old_dv_dt = dv_dt_list
        time = []
        this_voltage_column = []
        dv_dt_list = []
        for line in range(max_length):
            time.append(old_time[line])
            this_voltage_column.append(old_voltage[line])
            dv_dt_list.append(old_dv_dt[line])

        # Multibeat separation
        if skip_separation == False:
            try:
                # Finding upstrokes and rough ends of potentials
                for line in range(firstline,max_length):
                    if upstroke_found == False:
                        if dv_dt_list[line] > 0.6 * dv_max and abs(volt_min - this_voltage_column[line]) > 0.2 * abs(volt_min-volt_max):
                            upstroke_found = True
                            upstroke_times.append(time[line])
                            upstroke_lines.append(line)
                    if upstroke_found == True:
                        if dv_dt_list[line] < 2 or dv_dt_list[line] > -2:
                            if this_voltage_column[line] < 0.9 * volt_min:
                                upstroke_found = False
                
                # Setting zones of interest between upstrokes
                limits=[]
                for upstroke in range(len(upstroke_times)):
                    if upstroke < len(upstroke_times)-1:
                        limits.append([upstroke_times[upstroke],upstroke_times[upstroke+1]])
                    else:
                        limits.append([upstroke_times[upstroke], time[max_length-1]])
                
                # Finding lowest point between each upstroke                              
                looking_for = 0
                local_volt_min = 0
                local_volt_min_time = 0
                local_volt_max = -50
                upstroke_end_times = []
                for line in range(max_length):
                    if time[line] > limits[0][0]:
                        if limits[looking_for][0] < time[line] < limits[looking_for][1]:
                            if this_voltage_column[line] < volt_min-(volt_min*0.1):
                                if this_voltage_column[line] < local_volt_min:
                                    local_volt_min = this_voltage_column[line]
                                    local_volt_min_time = time[line]
                            if this_voltage_column[line] > local_volt_max:
                                local_volt_max = this_voltage_column[line]
                                local_volt_max_time = time[line]
                        else:
                            end_times.append(local_volt_min_time)
                            upstroke_end_times.append(local_volt_max_time)
                            local_volt_min = 0
                            local_volt_min_time = 0
                            local_volt_max = -50
                            looking_for = looking_for + 1
                
                # If more than 1 potential is found in a column, create separate potentials
                if len(end_times) > 2:
                    # Unseparated potentials will seem continuous, but will be made up of individual segments
                    out_unseparated = []
                    additional_info['MULTIBEAT'] = True
                    this_potential = []
                    this_unseparated_potential = []
                    this_time = []
                    this_voltage = []
                    this_deriv = []
                    unseparated_time = []
                    voltage_column = this_voltage_column
                    potential_number = 0
                    time_step = time[2] - time[1]
                    fromzero_time = 0
                    absolute_time = 0
                    if calculation_settings['slow_response'] == True:
                        for line in range(max_length):
                            if time[line] < end_times[potential_number]:
                                fromzero_time = fromzero_time + time_step
                                absolute_time = absolute_time + time_step
                                unseparated_time.append(absolute_time)
                                this_time.append(fromzero_time)
                                this_voltage.append(voltage_column[line])
                                this_deriv.append(dv_dt_list[line])
                            
                            if time[line] >= end_times[potential_number]:
                                fromzero_time = 0
                                if potential_number != 0:
                                    cycle_lengths.append(upstroke_times[potential_number] - upstroke_times[potential_number-1])
                                potential_number = potential_number + 1
                                this_potential.append(this_time)
                                this_potential.append(this_voltage)
                                this_potential.append(this_deriv)
                                out_potentials.append(this_potential)
                                this_unseparated_potential.append(unseparated_time)
                                this_unseparated_potential.append(this_voltage)
                                this_unseparated_potential.append(this_deriv)
                                this_unseparated_potential.append(acq_id)
                                out_unseparated.append(this_unseparated_potential)
                                this_potential = []
                                this_unseparated_potential = []
                                this_time = []
                                this_voltage = []
                                unseparated_time = []
                                if potential_number > len(end_times):
                                    break
                                
                            if len(upstroke_times) > len(end_times):
                                if time[line] > upstroke_times[len(upstroke_times)-1]:
                                    break
                            else:
                                if time[line] == end_times[len(upstroke_times)-1]:
                                    break
                        
                    else:    
                        for line in range(max_length):
                            #if time[line] < end_times[potential_number]:
                            if time[line] >= upstroke_times[potential_number] - 10:
                                fromzero_time = fromzero_time + time_step
                                absolute_time = absolute_time + time_step
                                unseparated_time.append(absolute_time)
                                this_time.append(fromzero_time)
                                this_voltage.append(voltage_column[line])
                                this_deriv.append(dv_dt_list[line])
                            
                            if time[line] >= end_times[potential_number]:
                                fromzero_time = 0
                                if potential_number != 0:
                                    cycle_lengths.append(upstroke_times[potential_number] - upstroke_times[potential_number-1])
                                potential_number = potential_number + 1
                                this_potential.append(this_time)
                                this_potential.append(this_voltage)
                                this_potential.append(this_deriv)
                                out_potentials.append(this_potential)
                                this_unseparated_potential.append(unseparated_time)
                                this_unseparated_potential.append(this_voltage)
                                this_unseparated_potential.append(this_deriv)
                                this_unseparated_potential.append(acq_id)
                                out_unseparated.append(this_unseparated_potential)
                                this_potential = []
                                this_unseparated_potential = []
                                this_time = []
                                this_voltage = []
                                unseparated_time = []
                                if potential_number > len(end_times):
                                    break
                                
                            if len(upstroke_times) > len(end_times):
                                if time[line] > upstroke_times[len(upstroke_times)-1]:
                                    break
                            else:
                                if time[line] == end_times[len(upstroke_times)-1]:
                                    break
                else:                
                    this_potential.append(time)
                    this_potential.append(this_voltage_column)
                    this_potential.append(dv_dt_list)
                    this_potential.append(acq_id)
                    out_potentials.append(this_potential)
                    
            # changes in data acquisition frequency lead to abnormal first derivative shape
            except:
                print('Beat separation not possible.')
                this_potential.append(time)
                this_potential.append(this_voltage_column)
                this_potential.append(dv_dt_list)
                this_potential.append(acq_id)
                out_potentials.append(this_potential)
                
        else:
            this_potential.append(time)
            this_potential.append(this_voltage_column)
            this_potential.append(dv_dt_list)
            this_potential.append(acq_id)
            out_potentials.append(this_potential)
    
    
    # Paired lists for output
    return out_potentials, out_unseparated, cycle_lengths, additional_info