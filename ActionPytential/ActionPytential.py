#!/usr/bin/env python3

"""
This is a Python 3 script for analyzing
and visualizing cardiac action potential data.

"""

__version__ = '0.8.3'


# External dependencies
import os 
import re
import csv
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
import random
import PySimpleGUI as sg
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg; matplotlib.use('TkAgg')
import math
import datetime
import numpy as np
import warnings
import yaml

try:
    from scipy import signal
    from scipy.signal import butter
except ModuleNotFoundError:
    scipy_missing = True # this might be the case on some ARM devices,
                         # filtering is skipped.
                         
# Script parts
from import_files import *                         
from ap_parameters import action_potential_parameters

global DEBUG; DEBUG = False
global BENCHMARK; BENCHMARK = False

def log(message):
    if DEBUG == True:
        print(message)

def benchmark(message):
    if BENCHMARK == True:
        print(message)

def main():
    if os.path.isfile('config.yml'):
        config = yaml.load(open('config.yml', 'r'),Loader=yaml.FullLoader)
    else:
        config = {'compact_view': False, 'dark_mode':  False}
    font = ("Arial", 9)
    if config['dark_mode'] == True:
        sg.ChangeLookAndFeel('Black')
    else:
        sg.ChangeLookAndFeel('Reddit')
    export_window_active = False
    average_window_active = False
    auto_average_window_active = False
    manual_parameters_window_active = False
    recalculate = True
    highlighted_rows = []
    # Checking screen size
    # A more compact view is used with vertical resolutions under 1080 pixels
    testwindow = sg.Window("Test")
    screenwidth, screenheight = testwindow.get_screen_size()
    testwindow.close()
    sg.set_options(auto_size_buttons=True)
    active_table = '-TABLE-CLASSIC-'
    warnings.filterwarnings('ignore')
    preview_variables = ('CT','APD90','APD75','APD50','APD25','APD10','dvmin_time','OST','PV20','PV50','PV75','NT','MDP','TOPT','RMP','APD')    
    
    if screenheight < 1080 or config['compact_view'] == True:
        terminal_width,terminal_height=50,4
        table_columns_width=6
        table_row_numbers=11
        fig_canvas_dpi=55
        file_browser_width,file_browser_height=34,15
        spacer='                          '
        window_resize = False
        config['compact_view'] = True
    else:
        terminal_width,terminal_height=62,7
        fig_canvas_dpi=85
        table_columns_width=7
        table_row_numbers=14
        file_browser_width,file_browser_height = 26,22
        spacer='                                   '
        window_resize = True

    wd_files = []  
    chosen_files = []
    chosen_filenames = []
    empty_table_data=[['               ', '              ', '', '', '', '', '', '', '', '', '', '', '','','', '', '', '', '', '', '', '', '', '', '','']]
    table_data = empty_table_data
    data_list=[]
   
    # Columns of the Main tab 
    upcolumn1=[[sg.Text('Working directory:', size=(16, 1), auto_size_text=False,
                  justification='right', font = font),
          sg.InputText(os.getcwd(), key='-WDIR-', size=(70,1)), sg.FolderBrowse(), sg.Button('Update',key='-UPD-',size=(6,1), font = font)],
         [sg.Text('ID database:', size=(16, 1), auto_size_text=False,
                  justification='right', font = font),
          sg.InputText(os.getcwd()+'/AP-database.csv', key='-WDB-',size=(70,1), font = font),
                  sg.FileBrowse()],
         [sg.Text('Filter expression:', size=(16, 1), auto_size_text=False,justification='right', font = font),
          sg.InputText('.*',key='-FILEREGEX-',size=(70,1)), sg.Button('Add', key='-BATCHADD-',button_color=('white', 'darkgreen'),size=(6,1), font = font),
          sg.Button('Remove', key='-BATCHREMOVE-',button_color=('white', 'darkred'),size=(6,1), font = font)]]
    
    upcolumn1_compact=[[sg.Text('Working directory:', size=(16, 1), auto_size_text=False,
                  justification='right', font = font),
          sg.InputText(os.getcwd(), key='-WDIR-', size=(80,1)), sg.FolderBrowse(), sg.Button('Update',key='-UPD-',size=(6,1), font = font)],
         [sg.Text('ID database:', size=(16, 1), auto_size_text=False,
                  justification='right', font = font),
          sg.InputText(os.getcwd()+'/AP-database.csv', key='-WDB-',size=(80,1), font = font),
                  sg.FileBrowse()]]
   
    upcolumn2=[[
       sg.Output(size=(terminal_width,terminal_height))
        ]]
    
    column1=[[sg.Text('Files in working directory: ', font = font)],
             [sg.Listbox(values=wd_files, size=(file_browser_width,file_browser_height), font = font, key='-FFILES-',
                         bind_return_key=True)]]
   
    column2=[[sg.Text('Chosen files: ', font = font)],
            [sg.Listbox(values=chosen_files, size=(file_browser_width,file_browser_height), font = font, key='-INFILES-',
                        bind_return_key=True)]]
   
    column3=[[sg.Frame('Plot settings',[
                [sg.Radio('Skip plot', "RADIO1", size=(15,2),key='-NOPLOT-', font = font)],
                [sg.Radio('Single potential', "RADIO1", size=(15,2), key='-SINGLEPLOT-', font = font)],
                [sg.Radio('No corrections', "RADIO1",default=True, size=(15,2), key='-NOCOR-', font = font)],
                [sg.Radio('RMP correction', "RADIO1", size=(15,2), key='-RMP-', font = font)],
                [sg.Radio('AMP correction', "RADIO1", size=(15,2), key='-AMP-', font = font)],
                [sg.Radio('First derivatives', "RADIO1", size=(15,2), key='-FDV-', font = font)],
                #[sg.Radio('Repolarization%', "RADIO1", size=(15,2), key='-REPOL-', disabled=True)],
                #[sg.Checkbox(' Dynamic colors', size=(15,2), key='-DCOL-',default=True)]
                ])],
           [sg.Frame('Resting potential',[
                [sg.Radio('Beginning', "RADIO3",size=(15,1), key='-RPMODE1-', enable_events=True, font = font)],
                [sg.Radio('Average', "RADIO3", size=(15,1), key='-RPMODE2-', enable_events=True, font = font)],
                [sg.Radio('End', "RADIO3", size=(15,1), key='-RPMODE3-', default=True, enable_events=True, font = font)],
                [sg.Radio('Voltage clamp', "RADIO3", size=(15,1), key='-RPMODE4-', enable_events=True, font = font)],
                [sg.Radio('Slow response', "RADIO3", size=(15,1), key='-RPMODE5-', enable_events=True, font = font)]])],
           [sg.Frame('Value to show on plot',[
                [sg.Combo(values=preview_variables, size=(10,1), readonly=True, key='-PLOTVALUE-', enable_events=True, font = font)]])]
                ]
   
    column4=[
        [sg.Frame('Continuous recordings',
               [[sg.Radio('Skip separation',"RADIO4", key = '-MB-SKIP-',default = True, enable_events=True, font = font)],
                [sg.Radio('Show original',"RADIO4", key ='-MB-ORIG-', default=False, enable_events=True, font = font)],
                [sg.Radio('Show stacked',"RADIO4", key = '-MB-STACKED-', default=False, enable_events=True, font = font)]])],
       
        [sg.Frame('Timeframe (ms)',
               [[sg.InputText('2.5', key='-DELAYTIME-', size=(5,1), font = font),sg.Text(' Beginning', font = font)],
                [sg.InputText('0', key='-ENDTIME-', size=(5,1), font = font),sg.Text(' End', font = font)]])],
        [sg.Frame('Butterworth filter',
                [[sg.Checkbox(' Apply filter', key='-BUTTERUSE-', enable_events=True, font = font)],
                 [sg.Slider(key='-BUTTERCUTOFF-', range=(11, 100), orientation='h', size=(15, 15), default_value=40, enable_events=True, font = font)],
                 [sg.Text('Filter cutoff (Hz)')],
              #  [sg.Slider(key='-BUTTERORDER-', range=(1, 10), orientation='h', size=(15, 15), default_value=5, enable_events=True, font = font)],
              #  [sg.Text('Filter order', font = font)]
                 ])],
        [sg.Frame('Units',[
                [sg.Text('Time', size=(9,1), font = font),
                sg.Combo(values=('us','ms','s'), default_value='ms', readonly=True, key='-TIMEUNIT-', size=(5,1), enable_events=True, font = font)],
                [sg.Text('Voltage', size=(9,1), font = font),
                sg.Combo(values=('pV','nV','uV','mV', 'V'), default_value='mV', readonly=True, key='-VOLTUNIT-', size=(5,1), enable_events=True, font = font)]
                ])]
                ]
    
    column3_compact=[[sg.Frame('Plot settings',[
            [sg.Combo(values=('Skip plot','Single potential','No corrections','RMP correction','AMP correction','First derivatives'), default_value='No corrections', key='RCOMBO1',readonly=True,enable_events=True,font = font)],
            #[sg.Radio('Repolarization%', "RADIO1", size=(15,2), key='-REPOL-', disabled=True)],
            ])],
       [sg.Frame('Resting potential',[
            [sg.Combo(values=('Beginning','Average','End','Voltage clamp', 'Slow response'), default_value ='End', key = 'RCOMBO3',readonly=True,enable_events=True,font = font)]])],
         [sg.Frame('Units',[
            [sg.Text('Time', size=(9,1), font = font),
            sg.Combo(values=('us','ms','s'), default_value='ms', readonly=True, key='-TIMEUNIT-', size=(5,1), enable_events=True, font = font)],
            [sg.Text('Voltage', size=(9,1), font = font),
            sg.Combo(values=('pV','nV','uV','mV', 'V'), default_value='mV', readonly=True, key='-VOLTUNIT-', size=(5,1), enable_events=True, font = font)]
            ])],
        [sg.Frame('Value to show on plot',[
                [sg.Combo(values=preview_variables, size=(10,1), readonly=True, key='-PLOTVALUE-', enable_events=True, font = font)]])]
         ]
   
    column4_compact=[
        [sg.Frame('Continuous recordings',[
            [sg.Combo(values=('Skip separation','Show original','Show stacked'), default_value = 'Skip separation',readonly=True,enable_events=True,key='RCOMBO4',font = font)]])],
        [sg.Frame('Timeframe (ms)',
               [[sg.InputText('1.5', key='-DELAYTIME-', size=(5,1), font = font),sg.Text(' Beginning', font = font)],
                [sg.InputText('0', key='-ENDTIME-', size=(5,1), font = font),sg.Text(' End', font = font)]])],
        [sg.Frame('Butterworth filter',
                [[sg.Checkbox(' Apply filter', key='-BUTTERUSE-', enable_events=True, font = font)],
                 [sg.Slider(key='-BUTTERCUTOFF-', range=(11, 100), orientation='h', size=(15, 15), default_value=40, enable_events=True, font = font)],
                 [sg.Text('Filter cutoff (Hz)')],
                 #[sg.InputText('5', key='-BUTTERORDER-', size=(5,1), font = font),sg.Text(' Filter order', font = font)],
                 ])]
                ]
    
    column5 = [[sg.Canvas(key='-CANVAS-')]]
    
    table_layout_conventional = [[sg.Table(values=table_data,
                       headings=['MEASUREMENT','ID','  CL  ',' TRAIN  ',' CT ','RMP ',' AMP ','dVmax ','dVmin ','APD90 ','APD75 ',
              'APD50 ','APD25 ','APD10 ', 'dVminT '],
                       auto_size_columns=True,
                       display_row_numbers=False,
                       num_rows=table_row_numbers,
                       justification='center',
                       key='-TABLE-CLASSIC-',
                       font = font,
                       enable_events = True)]]
    
    table_layout_slowresponse = [[sg.Table(values=table_data,
                       headings=['MEASUREMENT','ID','  CL  ','OSV ','OST ','PV20', 'PV20%', 'PV50', 'PV50%', 'PV75', 'PV75%','MPS ' ,' TR  ',  'NdVmin ', ' NV ', ' NT ', 'NV-R ', 'NV-R% '],
                       auto_size_columns=True,
                       display_row_numbers=False,
                       num_rows=table_row_numbers,
                       justification='center',
                       key='-TABLE-NP-',
                       font = font,
                       enable_events = True)]]
    
    table_layout_voltageclamp = [[sg.Table(values=table_data,
                       headings=['MEASUREMENT','ID','  CL  ',' CT ',' MDP ','TOPV ','TOPT ',' EDD ',' LDD ',' RMP ',' AMP ',' APD '],
                       auto_size_columns=True,
                       display_row_numbers=False,
                       num_rows=table_row_numbers,
                       justification='center',
                       key='-TABLE-SR-',
                       font = font,
                       enable_events = True)]]
    
    # Main window
    if config['compact_view'] == False:
        main_layout = [
                 [sg.Column(upcolumn1),sg.Column(upcolumn2)
                  ],
                 [sg.TabGroup([[sg.Tab('Classic', table_layout_conventional),
                            sg.Tab('Notch/Plateau', table_layout_slowresponse),
                            sg.Tab('Slow response', table_layout_voltageclamp),]])],                             
                 [sg.Column(column1),sg.Column(column2),sg.Column(column3),sg.Column(column4),sg.Column(column5)],
                 [sg.Button('Start', key='-START-', size=(12,1),
                            button_color=('white', 'green'), disabled=True, font = font),
                  sg.Button('Matplotlib', key='-MATPLOT-', button_color=('white', 'MediumPurple2'),
                            size=(12, 1), disabled=True, font = font),
                  sg.Button('Manual analysis', key='-MANUAL-', button_color=('white', 'black'),
                            size=(12, 1), disabled=True, font = font),
                  sg.Button('Export', key='-EXPORT-',
                            size=(12, 1), disabled=True, font = font),
                  sg.Button('Average', key='-AVERAGE-', button_color=('white', 'orange'),
                            size=(12, 1), disabled=True, font = font),
                  sg.Button('AutoAverage', key='-AUTOAVERAGE-', button_color=('white', 'tomato1'),
                            size=(12, 1), disabled=True, font = font),
                  sg.Text(spacer),
                  sg.Text(spacer),
                  sg.Text(spacer),
                  sg.Button('Reset', key='-RESET-', button_color=('white', 'red'),
                            size=(12, 1), font = font),
                  ]]
    else:
        main_layout = [
             [sg.Column(upcolumn1_compact),sg.Column(upcolumn2)
              ],
             [sg.TabGroup([[sg.Tab('Classic', table_layout_conventional),
                            sg.Tab('Notch/Plateau', table_layout_slowresponse),
                            sg.Tab('Slow response', table_layout_voltageclamp),]]),
              sg.Column([[sg.Text('File filter expression:', size=(16, 1), auto_size_text=False,justification='right', font = font)],
          [sg.InputText('.*',key='-FILEREGEX-',size=(20,1))],
           [sg.Button('Add', key='-BATCHADD-',button_color=('white', 'darkgreen'),size=(6,1), font = font),
          sg.Button('Remove', key='-BATCHREMOVE-',button_color=('white', 'darkred'),size=(6,1), font = font)]])],                                             
             [sg.Column(column1),sg.Column(column2),sg.Column(column3_compact),sg.Column(column4_compact),sg.Column(column5)],
             [sg.Button('Start', key='-START-', size=(12,1),
                        button_color=('white', 'green'), disabled=True, font = font),
              sg.Button('Matplotlib', key='-MATPLOT-', button_color=('white', 'MediumPurple2'),
                        size=(12, 1), disabled=True, font = font),
              sg.Button('Manual analysis', key='-MANUAL-', button_color=('white', 'black'),
                        size=(12, 1), disabled=True, font = font),
              sg.Button('Export', key='-EXPORT-',
                        size=(12, 1), disabled=True, font = font),
              sg.Button('Average', key='-AVERAGE-', button_color=('white', 'orange'),
                        size=(12, 1), disabled=True, font = font),
              sg.Button('AutoAverage', key='-AUTOAVERAGE-', button_color=('white', 'tomato1'),
                        size=(12, 1), disabled=True, font = font),
              sg.Text(spacer),
              sg.Text(spacer),
              sg.Text(spacer),
              sg.Button('Reset', key='-RESET-', button_color=('white', 'red'),
                        size=(12, 1), font = font),
              ]]
   
    # DB creation
    db_column1 = [
             [sg.Frame('Date',
                [[sg.Input('200212AF', size=(9,1), key='-DATE-'),
                  sg.Checkbox('', key= '-HDATE-')]])],
             
             [sg.Frame('Preparation',
                [[sg.Text('Model: '), sg.Input('Rabbit', size=(13,1), key='-PREP-'),
                  sg.Checkbox('', key= '-HPREP-')
                  ],
                 [sg.Text('Count: '), sg.Input('1', size=(3,1), key='-COUNT-')                  ]
                 ])],
             
            [sg.Frame('Cycle length',
                [[sg.Input('1000', size=(21,1), key='-BCL-'),
                  sg.Checkbox('', key= '-HBCL-')
                  ]])],
             
             
             [sg.Frame('Drugs',[
                 [sg.Text('File', size=(4,1)),sg.Text('Name', size=(15,1)),
                  sg.Checkbox('', key= '-HDRUGS-')],
                 [sg.Input('1', size=(4,1),key='-DNUM0-'),
                  sg.Input('Control',size=(20,1), key='-DNAME0-')],
                 [sg.Input('3', size=(4,1),key='-DNUM1-'),
                  sg.Input('Drug 1',size=(20,1), key='-DNAME1-')],
                 [sg.Input('10', size=(4,1),key='-DNUM2-'),
                  sg.Input('Drug 2',size=(20,1), key='-DNAME2-')],         
                 [sg.Input('15', size=(4,1),key='-DNUM3-'),
                  sg.Input('Drug 3',size=(20,1), key='-DNAME3-')],
                 [sg.Input('', size=(4,1),key='-DNUM4-'),
                  sg.Input('',size=(20,1), key='-DNAME4-')],
                 [sg.Input('', size=(4,1),key='-DNUM5-'),
                  sg.Input('',size=(20,1), key='-DNAME5-')]
               ])],
               
               [sg.Frame('Last file', [[sg.Input('40', size=(4,1),key='-LASTF-')]])]
                ]
    
    db_column2= [[sg.Frame('Restitution',[
                   [sg.Text('File', size=(3,1)),sg.Text('APD90+CT', size=(10,1)),],
                   [sg.Input('9', size=(3,1),key='-RNUM0-'),
                  sg.Input('177',size=(7,1), key='-RTIME0-')],
                   [sg.Input('17', size=(3,1),key='-RNUM1-'),
                  sg.Input('221',size=(7,1), key='-RTIME1-')],
                   [sg.Input('', size=(3,1),key='-RNUM2-'),
                  sg.Input('',size=(7,1), key='-RTIME2-')],
                   [sg.Input('', size=(3,1),key='-RNUM3-'),
                  sg.Input('',size=(7,1), key='-RTIME3-')],
                   [sg.Input('', size=(3,1),key='-RNUM4-'),
                  sg.Input('',size=(7,1), key='-RTIME4-')],
                   ])],
               [sg.Frame('Other protocols',[
                   [sg.Text('Files [1,2,3]', size=(10,1)),sg.Text('Names', size=(6,1)),
                    sg.Checkbox('', key= '-HPROT-')],
                   [sg.Input('2,3,5', size=(10,1),key='-PNUM0-'),
                  sg.Input('CL',size=(10,1), key='-PNAME0-')],
                 [sg.Input('4,7,23', size=(10,1),key='-PNUM1-'),
                  sg.Input('ONSET',size=(10,1), key='-PNAME1-')],
                 [sg.Input('10', size=(10,1),key='-PNUM2-'),
                  sg.Input('VARIAB',size=(10,1), key='-PNAME2-')],         
                 [sg.Input('', size=(10,1),key='-PNUM3-'),
                  sg.Input('',size=(10,1), key='-PNAME3-')],
                   [sg.Input('', size=(10,1),key='-PNUM4-'),
                  sg.Input('',size=(10,1), key='-PNAME4-')],
                 [sg.Input('', size=(10,1),key='-PNUM5-'),
                  sg.Input('',size=(10,1), key='-PNAME5-')],         
                 [sg.Input('', size=(10,1),key='-PNUM6-'),
                  sg.Input('',size=(10,1), key='-PNAME6-')]
                   ])]
             ]
    
    db_layout=[[sg.Frame('ID database',
                         [[sg.Text('Select an existing database file (otherwise a new one is created):')],
                          [sg.Input('', key = '-DB-OUTFILE-', size = (85,1)),sg.FileBrowse()]])],
            [sg.Column(db_column1),sg.Column(db_column2)],
            [sg.Text('')],
            [sg.Text('', key = '-DB-INFO-')],
            [   sg.Button('Generate', key='-CREATE-DB-'),
                sg.Button('Reset', key='-RESET-DB-',button_color=('white', 'red'))
             ]]
    
    layout = [[sg.TabGroup([[sg.Tab('Action potential analysis', main_layout),
                             sg.Tab('ID database creation', db_layout)]])]]
   
    window = sg.Window('ActionPytential v' + __version__, layout, resizable=window_resize,finalize=True)
    fig_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas, plot_empty_figure(fig_canvas_dpi))
   
    # Update window with working directory
    wd_list = []
    current_wd = os.getcwd()
    wd_list.append(current_wd)
    current_wd_no = 0
    wd_files = []
    wd_all = os.listdir(os.getcwd())
    wd_all.sort()
    for file in range(len(wd_all)):
        if os.path.isfile(wd_all[file]): wd_files.append(wd_all[file])  
    window['-FFILES-'].update(values=wd_files)
   
    # Print welcome message
    print('Welcome to ActionPytential v'+__version__)
    print('Choose the working directory with the top Browse button.')
    print('Then press Update.')
        
        
    # Window event loop
    manual_data_temp = []
    manual_data_list = []

    if 'scipy_missing' in locals() and scipy_missing == True:
        window['-BUTTERUSE-'].update(disabled = True)
      # window['-BUTTERORDER-'].update(disabled = True)
        window['-BUTTERCUTOFF-'].update(disabled = True)
    
    first = True
    while True:            
        event, values = window.read(timeout = 100)
        #log(event, values)
        if event in (sg.WIN_CLOSED, 'Exit'):
            break
            
        elif event == '-UPD-':
            wd_files=[]
            if values['-WDIR-']=='': values['-WDIR-']=values[1]
            try:
                os.chdir(values['-WDIR-'])
                current_wd = os.getcwd()
                wd_list.append(current_wd)
                current_wd_no = current_wd_no + 1
            except FileNotFoundError:
                print('Chosen directory not found. Please, check the location.')
            wd_files = []
            wd_all = os.listdir(os.getcwd())
            wd_all.sort()
            for file in range(len(wd_all)):
                if os.path.isfile(wd_all[file]): wd_files.append(wd_all[file])  
            window['-FFILES-'].update(values=wd_files)
            if first:
                print('Now you can choose the input files by double-clicking them in the left pane.')
            
        elif event == '-FFILES-':
            recalculate = True
            if chosen_files == []:
                new_file = {'filename': values['-FFILES-'][0],'wd': current_wd_no}
                chosen_files.append(new_file)
                display_filenames = []
                for file in range(len(chosen_files)):
                    display_filenames.append(chosen_files[file]['filename'])
                window['-INFILES-'].update(values=display_filenames)
                window['-START-'].update(disabled=False) 
            else:
                for file in range(len(chosen_files)):
                    if chosen_files[file]['filename'] == values['-FFILES-'][0]:
                        print(values['-FFILES-'][0],'duplicate filename.')
                        break
                    if file+1 == len(chosen_files):
                        new_file = {'filename': values['-FFILES-'][0], 'wd': current_wd_no}
                        chosen_files.append(new_file)
                        display_filenames = []
                        for file in range(len(chosen_files)):
                            display_filenames.append(chosen_files[file]['filename'])
                        window['-INFILES-'].update(values=display_filenames)
                        window['-START-'].update(disabled=False)
            if first:
                print('Press Start to process the chosen files.')
                first = False
                
        elif event == '-INFILES-':
            recalculate = True
            for file in range(len(chosen_files)):
                try:
                    if chosen_files[file]['filename'] == values['-INFILES-'][0]:
                        del chosen_files [file]
                        display_filenames = []
                        for file in range(len(chosen_files)):
                            display_filenames.append(chosen_files[file]['filename'])
                            window['-INFILES-'].update(values=display_filenames)
                        break
                except:
                    pass
            if chosen_files == []:
                window['-INFILES-'].update(values=[])
        elif event == '-RESET-':
            manual_data_list = []
            chosen_files = []
            window['-INFILES-'].update(values=chosen_files)
            window['-TABLE-CLASSIC-'].update(values=empty_table_data)
            window['-START-'].update(disabled=True)
            window['-MANUAL-'].update(disabled=True)
            window['-MATPLOT-'].update(disabled=True)
            window['-EXPORT-'].update(disabled=True)
            window['-AVERAGE-'].update(disabled=True)
            window['-AUTOAVERAGE-'].update(disabled=True)
            window['-DELAYTIME-'].update(1.5)
            window['-ENDTIME-'].update(0)
            window['-BUTTERUSE-'].update(False)
            window['-BUTTERCUTOFF-'].update(50)
          # window['-BUTTERORDER-'].update(5)
            if fig_canvas_agg != None:
                delete_fig_agg(fig_canvas_agg)
            fig_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas, plot_empty_figure(fig_canvas_dpi))
            from_popup = False
            print('Default settings loaded.')
            
        elif event == '-BATCHADD-':
            recalculate = True
            re_input_files=[]
            try:
                files_re = re.compile(str(values['-FILEREGEX-']))
                for file in range(len(wd_files)):
                    match = files_re.search(wd_files[file])
                    if match:
                        re_input_files.append(wd_files[file])
            except:
                print('Invalid filter expression.')
            
            if re_input_files !=[]:
                for file in range(len(re_input_files)):
                    chosen_files.append({'filename':re_input_files[file], 'wd': current_wd_no})
                display_filenames = []
                for file in range(len(chosen_files)):
                    display_filenames.append(chosen_files[file]['filename'])
                window['-INFILES-'].update(values=display_filenames)
                window['-START-'].update(disabled=False)
                
        elif event == '-BATCHREMOVE-':
            recalculate = True
            re_input_files=[]
            try:
                files_re = re.compile(str(values['-FILEREGEX-']))
                for file in range(len(chosen_files)):
                    match = files_re.search(chosen_files[file]['filename'])
                    if match:
                        re_input_files.append(chosen_files[file])
            except:
                print('Invalid filter expression.')
            
            if re_input_files !=[]:
                for file in range(len(re_input_files)):
                    chosen_files.remove(re_input_files[file])
                display_filenames = []
                if chosen_files != []:
                    for file in range(len(chosen_files)):
                        display_filenames.append(chosen_files[file]['filename'])
                        window['-INFILES-'].update(values=display_filenames)
                else:
                    window['-INFILES-'].update(values=[])
                window['-START-'].update(disabled=False)        
        
        elif event == '-START-' or str(event) == 'Return:36':
            very_start_time = datetime.datetime.now()
            #log(values)
            
            # Comboboxes replace the radios in compact view
            # These variables need to be replaced in the values variable
            if config['compact_view'] == True:
                values['-BUTTERORDER-'] = 5
                plot_settings_pairs = ['RCOMBO1',[['Skip plot','-NOPLOT-'],['Single potential','-SINGLEPLOT-'],['No corrections','-NOCOR-'],['RMP correction','-RMP-'],['AMP correction','-AMP-'],['First derivatives','-FDV-']]]
                resting_potential_pairs = ['RCOMBO3',[['Beginning','-RPMODE1-'],['Average','-RPMODE2-'],['End','-RPMODE3-'],['Voltage clamp','-RPMODE4-'],['Slow response','-RPMODE5-']]]
                continuous_recording_pairs = ['RCOMBO4',[['Skip separation','-MB-SKIP-'],['Show original','-MB-ORIG-'],['Show stacked','-MB-STACKED-']]]
                for pairs in (plot_settings_pairs, resting_potential_pairs,continuous_recording_pairs):
                    key = pairs[0]
                    pairs_list = pairs[1]
                    for pair in pairs_list:
                        if key in values:
                            if values[key] == pair[0]:
                                values[pair[1]] = True
                            else:
                                values[pair[1]] = False
                        else:
                            values[pair[1]] = False
            
            if len(chosen_files) > 0:
                if values[active_table] != []:
                    chosen_table_rows = values[active_table]
                    for row in range(len(chosen_table_rows)):
                        highlighted_rows.append((chosen_table_rows[row], 'white', row_color))
                    if from_popup == True:
                        chosen_table_rows = [0]
                else:
                    chosen_table_rows = [0]
                title = ''
                db_file = values['-WDB-']                
                
                # Calculation settings
                calculation_settings = {}
                calculation_settings['delaytime'] = float(values['-DELAYTIME-'])
                calculation_settings['endtime'] = float(values['-ENDTIME-'])
                calculation_settings['filter_cutoff'] = values['-BUTTERCUTOFF-']
                calculation_settings['filter_order'] = 5 # values['-BUTTERORDER-']
                calculation_settings['filter_use'] = values['-BUTTERUSE-']
                calculation_settings['time_unit'] = values['-TIMEUNIT-']
                calculation_settings['volt_unit'] = values['-VOLTUNIT-']
                calculation_settings['voltage_clamp'] = False
                calculation_settings['slow_response'] = False
                if values['-RPMODE1-'] == True:
                    calculation_settings['rp_mode'] = 'begin'
                elif values['-RPMODE2-'] == True:
                    calculation_settings['rp_mode'] = 'aver'
                elif values['-RPMODE4-'] == True:
                    calculation_settings['rp_mode'] = 'end'
                    calculation_settings['voltage_clamp'] = True
                elif values['-RPMODE5-'] == True:
                    calculation_settings['rp_mode'] = 'sr'
                    calculation_settings['slow_response'] = True
                else:
                    calculation_settings['rp_mode'] = 'end'            
                # Checking for file format
                # Files with comma, tab, space, and semicolon separation are supported
                valid_files=[]
                invalid_files=[]
                for file in range(len(chosen_files)):
                    try:
                        input_file = wd_list[chosen_files[file]['wd']]+'/'+str((chosen_files)[file]['filename'])
                        delimiter, decimal_comma = check_file(input_file)
                    except TypeError:
                        delimiter = False
                    if delimiter != False:
                        current_file = {}
                        current_file['filename']=str((chosen_files)[file]['filename'])
                        current_file['filedir']=wd_list[chosen_files[file]['wd']]
                        current_file['wd']=chosen_files[file]['wd']
                        current_file['delimiter']=delimiter
                        valid_files.append(current_file)
                    else:
                        print(chosen_files[file]['filename'], 'contained no action potentials.')
                        invalid_files.append(chosen_files[file])
                
                for chosen in range(len(chosen_files)):
                    try:
                        for file in range(len(invalid_files)):
                            if str(invalid_files[file]['filename']) == str(chosen_files[chosen]['filename']):
                                del chosen_files [chosen]
                    except IndexError:
                        pass
                display_filenames = []
                for file in range(len(chosen_files)):
                    display_filenames.append(chosen_files[file]['filename'])
                window['-INFILES-'].update(values=display_filenames)
 
                # Extraction of action potentials from each file
                if recalculate == True:
                    potentials_list = []
                    data_list = []
                    unseparated_potentials_list = []
                    benchmark('File input')
                    start_time = datetime.datetime.now()
                    for file in range(len(valid_files)):
                        new_potentials, new_unseparated_potentials, cycle_lengths, new_potentials_info = read_potentials(valid_files[file], valid_files[file]['delimiter'], decimal_comma, db_file, values['-MB-SKIP-'], calculation_settings)
                        lastnumber = 0
                        for potential in range(len(new_potentials)):
                            potentials_list.append(new_potentials[potential])
                            data_dict_temp = {}
                            data_dict_temp['filename'] = valid_files[file]['filename']
                            data_dict_temp['filedir'] = valid_files[file]['filedir']
                            data_dict_temp['wd'] = valid_files[file]['wd']
                            if len(cycle_lengths) > 0:
                                data_dict_temp['CL'] = cycle_lengths[potential]
                            data_dict_temp['ID'] = new_potentials_info['ID']+'.'+str(potential+1)
                            data_dict_temp['MEAS'] = new_potentials_info['MEAS']
                            data_dict_temp['TRAIN'] = new_potentials_info['TRAIN']
                            data_dict_temp['MULTIBEAT'] = new_potentials_info['MULTIBEAT']
                            data_dict_temp['x'] = new_potentials[potential][0]
                            data_dict_temp['y'] = new_potentials[potential][1]
                            data_dict_temp['fdv'] = new_potentials[potential][2]
                            data_list.append(data_dict_temp)
                            lastnumber = lastnumber + potential
                        
                        for potential in range(len(new_unseparated_potentials)):
                            unseparated_potential = {}
                            unseparated_potential['x'] = new_unseparated_potentials[potential][0]
                            unseparated_potential['y'] = new_unseparated_potentials[potential][1]
                            unseparated_potential['fdv'] = new_unseparated_potentials[potential][2]
                            unseparated_potential['ID'] =  ""#new_unseparated_potentials[potential][3] #new_potentials_info['ID']+'.'+str(potential+1)
                            unseparated_potentials_list.append(unseparated_potential)
                            
                    benchmark(datetime.datetime.now()-start_time) 
                    if len(data_list) > 0:
                        start_calc=datetime.datetime.now()
                        for element in range(len(data_list)):
                            # Calculating action potential parameters
                            potential_parameters = action_potential_parameters(data_list[element]['x'], data_list[element]['y'],
                                                                               data_list[element]['fdv'], calculation_settings)
                            if potential_parameters != None:
                                data_list[element]['validity'] = True
                                for key in potential_parameters:
                                    data_list[element][key] = potential_parameters[key]
                                title = data_list[0]['filename']
                            else:
                                print(str((data_list)[element][0]), 'is not compatible.')
                                data_list[element]['validity'] = False
                                try:
                                    chosen_files.remove(str((data_list)[element][0]))
                                except ValueError:
                                    pass
                                for file in range(len(chosen_files)):
                                    display_filenames.append(chosen_files[file]['filename'])
                                window['-INFILES-'].update(values=display_filenames)        
                        benchmark('AP parameters:')
                        benchmark(datetime.datetime.now()-start_calc)
                    else:
                        print('No valid files provided.')
                        print()
                    
                start_time = datetime.datetime.now()
                
                # Table operations
                table_data_classic=[]
                table_data_np=[]
                table_data_sr=[]
                
                classic_keys = ('CL','TRAIN','CT','RMP','AMP','dvmax','dvmin','APD90','APD75','APD50',
                                'APD25','APD10','dvmin_time')
                np_keys = ('CL', 'OSV','OST','PV20', 'PV20%', 'PV50', 'PV50%', 'PV75', 'PV75%','MPS', 'TR', 'NdVmin', 'NV', 'NT', 'NV-R', 'NV-R%')
                sr_keys = ('CL', 'CT', 'MDP', 'TOPV', 'TOPT', 'EDD', 'LDD', 'RMP', 'AMP', 'APD')
                
                def table_line_update(key, element, data_list):
                    try:    
                        if isinstance(data_list[element][key], str) == True:
                            return data_list[element][key]
                        else:
                            return str(round(data_list[element][key],1))
                    except KeyError:
                        return 'NA'
                
                for element in range(0,len(data_list)):
                    manual_match = False
                    if data_list[element]['validity'] == True:
                        table_line_classic=[]
                        table_line_np=[]
                        table_line_sr=[]
                        for i in range(len(manual_data_list)):
                            if manual_data_list[i]['ID_OLD'] == data_list[element]['ID']:
                                manual_element = i
                                manual_match = True
                                
                        for key in ('MEAS', 'ID'):
                            table_line_classic.append(data_list[element][key])
                            table_line_np.append(data_list[element][key])
                            table_line_sr.append(data_list[element][key])
                        
                        if manual_match == False:                            
                            for table in (table_line_classic, classic_keys), (table_line_np, np_keys), (table_line_sr, sr_keys):
                                for key in table[1]:
                                    table[0].append(table_line_update(key, element, data_list))
                            
                        else:
                            for table in (table_line_classic, classic_keys), (table_line_np, np_keys), (table_line_sr, sr_keys):
                                for key in table[1]:
                                    table[0].append(table_line_update(key, manual_element, manual_data_list))
                                    
                        table_data_classic.append(table_line_classic)
                        table_data_np.append(table_line_np)
                        table_data_sr.append(table_line_sr)
                
                window['-TABLE-CLASSIC-'].update(values=table_data_classic)
                window['-TABLE-NP-'].update(values=table_data_np)
                window['-TABLE-SR-'].update(values=table_data_sr)
                
                if highlighted_rows != []:
                    window['-TABLE-CLASSIC-'].update(row_colors=highlighted_rows)
                    highlighted_rows = []
                else:
                    for row in range(len(table_data)):
                        highlighted_rows = []
                        highlighted_rows.append(([row], 'black', 'white'))
                    window['-TABLE-CLASSIC-'].update(row_colors=highlighted_rows)
                benchmark('Table')
                benchmark(datetime.datetime.now()-start_time)
                start_time=datetime.datetime.now()
                
                # Corrections & plot creation
                if values['-NOPLOT-'] != True:
                    start_time = datetime.datetime.now()
                    if len(data_list) < 6: # small color list
                        color_list=['black', 'red', 'blue', 'green', 'purple'] 
                    else: # dynamic colorlist
                        color_list = colorgen(len(data_list)+1)
                    benchmark('Colors')
                    benchmark(datetime.datetime.now()-start_time)
                    
                    if len(chosen_table_rows) > 1 or values ['-SINGLEPLOT-'] == True:
                        selected_data_list = []
                        selected_unseparated_potentials_list = []
                        correct_to = chosen_table_rows[0]
                        for pot in range(len(chosen_table_rows)):
                            try:
                                selected_data_list.append(data_list[chosen_table_rows[pot]])
                                selected_unseparated_potentials_list.append(unseparated_potentials_list[chosen_table_rows[pot]])
                            except IndexError:
                                pass
                    else:
                        selected_data_list = data_list
                        selected_unseparated_potentials_list = unseparated_potentials_list
                    title = title
                    
                    if len(chosen_table_rows) == 1:
                        correct_to = chosen_table_rows[0]
                    
                    if len(potentials_list) >= 1:
                        if values['-RMP-'] == True:
                            correction_mode = 'mdp'
                        elif values['-AMP-'] == True:
                            correction_mode = 'amp'
                        elif values ['-FDV-'] == True:
                            correction_mode = 'fdv'
                        #elif values ['-REPOL-'] == True:
                           # correction_mode = 'repol'
                        else:
                            correction_mode = 'none'

                        if data_list[0]['MULTIBEAT'] == True and values['-MB-ORIG-'] == True:
                            data_to_plot = selected_unseparated_potentials_list
                        else:
                            data_to_plot = selected_data_list

                        print('Calculating plots for', len(data_to_plot), 'potential(s)...')
                        x, y = create_plot_data(data_to_plot, correction_mode, correct_to)
                        
                        # Add scatter plot overlay for variable to preview
                        variable_scatter = []
                        variable_to_plot = values['-PLOTVALUE-']
                        if variable_to_plot != "":
                            for potential in range(len(data_to_plot)):
                                for entry in range(len(data_list)):
                                    try:
                                        if variable_to_plot in ('RMP', 'MDP', 'PV','NV', 'PV20', 'PV50', 'PV75'):
                                             value_to_plot = data_list[entry][variable_to_plot]
                                             time_value = False
                                        elif variable_to_plot in ('TOPT', 'dvmin_time', 'CT'): # absolute values, no CT correction
                                            value_to_plot = data_list[entry][variable_to_plot]
                                            time_value = True
                                        else:  
                                            value_to_plot = data_list[entry][variable_to_plot] + data_list[entry]['CT']
                                            time_value = True
                                        if data_list[entry]['ID'] == data_to_plot[potential]['ID']:
                                            if time_value == True:
                                                scatter_x, scatter_y = variable_to_scatter(value_to_plot, x[potential], y[potential], time_value)
                                            else:
                                                scatter_x, scatter_y = variable_to_scatter(value_to_plot, y[potential], x[potential], time_value)
                                            variable_scatter.append({'x': scatter_x, 'y': scatter_y, 'name': variable_to_plot, 'value': value_to_plot})
                                    except:
                                        log(str('creating variable_scatter failed'))
                                        pass
                        
                    # Remove current figure and plot new one
                    if fig_canvas_agg != None:
                        delete_fig_agg(fig_canvas_agg)
                    fig_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas,
                                                 plot_canvas(x, y, color_list, data_to_plot, fig_canvas_dpi, variable_scatter))
                    
                    benchmark('Plots')
                    benchmark(datetime.datetime.now()-start_time)
                    window['-MATPLOT-'].update(disabled=False)
                    window['-MANUAL-'].update(disabled=False)
                    window['-EXPORT-'].update(disabled=False)
                    window['-AVERAGE-'].update(disabled=False)
                    window['-AUTOAVERAGE-'].update(disabled=False)
                     
                else:
                    if  table_data != []:
                        window['-EXPORT-'].update(disabled=False)
                    if fig_canvas_agg != None:
                        delete_fig_agg(fig_canvas_agg)
                        fig_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas, plot_empty_figure(fig_canvas_dpi))
                
                print('Finished without errors.')
                benchmark(datetime.datetime.now()-very_start_time)
                recalculate = False
                from_popup = False
                row_color = 'darkblue'

        # Repeated lines for auto_average_window and average_window are needed for PySimpleGUI
        elif not export_window_active and event == '-EXPORT-':
            suggested_filename_table = 'AP_' + data_list[0]["ID"][:8]+'_table'
            suggested_filename_plot = 'AP_' + data_list[0]["ID"][:8]+'_plots'
            export_window_active = True
            export_window_layout = [
                [sg.Text('Filenames to use:')],
                [sg.Checkbox('Table: ',key='-WRITETABLE-', default=True),sg.Input(suggested_filename_table,key='-INT-')],
                [sg.Checkbox('Plots: ',key='-WRITEPLOT-', default=False),sg.Input(suggested_filename_plot,key='-INP-')],
                [sg.Button('Export', key ='-WRITE-'), sg.Button('Cancel')]]
            export_window = sg.Window('Export', export_window_layout)
        
        if export_window_active:
            event, values = export_window.read(timeout=100)
            if event != sg.TIMEOUT_KEY:
                pass
            if event == '-WRITE-':
                if values['-WRITETABLE-'] == True:
                    filenum = 1
                    while (os.path.exists(os.path.abspath(values['-INT-']+'_'+str(filenum)+'.csv'))):
                        filenum+=1
                    table_filename = values['-INT-']+'_'+str(filenum)+'.csv'
                    if manual_data_list != []:
                        for element in range(len(manual_data_list)):
                            data_list.append(manual_data_list[element])
                    export_stats(data_list, table_filename)
                     
                if values['-WRITEPLOT-'] == True:
                    filenum = 1
                    while (os.path.exists(os.path.abspath(values['-INP-']+'_'+str(filenum)+'.csv'))):
                        filenum+=1
                    potential_filename = values['-INP-']+'_'+str(filenum)+'.csv'
                    export_potentials(len(x), x, y, data_to_plot, potential_filename)
                export_window_active = False
                export_window.close()
            
            if event == 'Cancel' or event == sg.WIN_CLOSED:
                export_window_active = False
                export_window.close()
        
        elif event == '-DCOL-' or event == '-BUTTERUSE-' or event == '-BUTTERORDER-' or event == '-BUTTERCUTOFF-' \
        or event == '-TIMEUNIT-' or event == '-VOLTUNIT-' or event == '-MB-ORIG-' or event == '-MB-STACKED-' or event == '-MB-SKIP-' \
        or event == '-RPMODE1-' or event == '-RPMODE2-' or event == '-RPMODE3-' or event == '-RPMODE4-' or event == '-RPMODE5-'\
        or event == 'RCOMBO1' or event == 'RCOMBO2' or event == 'RCOMBO3' or event == 'RCOMBO4':
            recalculate = True
        
        elif event == '-TABLE-CLASSIC-':
            active_table = '-TABLE-CLASSIC-'
        
        elif event == '-TABLE-NP-':
            active_table = '-TABLE-NP-'
    
        elif event == '-TABLE-SR-':
            active_table = '-TABLE-SR-'

        elif event == '-MATPLOT-':
            if manual_parameters_window_active == True:
                plot_window(xm, ym, title, color_list, manual_data, [])
            else:
                plot_window(x, y, title, color_list, data_to_plot, variable_scatter)
        
        elif not manual_parameters_window_active and event == '-MANUAL-':
            chosen_table_rows = values['-TABLE-CLASSIC-']
            
            if len(chosen_table_rows) > 1 or len(chosen_table_rows) == 0:
                print('Choose a single potential for manual analysis.')
                
            else:
                manual_chosen_row = chosen_table_rows[0]
                manual_data = []
                manual_data.append(data_list[manual_chosen_row])
                xm, ym = create_plot_data(manual_data)
                window.write_event_value('-START-', '')
                title = manual_data[0]['ID']
                window.write_event_value('-MATPLOT-', '')
                correction_dict = {}
                for key in ('APD90','APD75','APD50','APD25','APD10','PT','NT'):
                    correction_dict[key] = 0
                manual_apd90 = manual_apd75 = manual_apd50 = manual_apd25 = manual_apd10 = ''
                manual_amp = ''
                manual_parameters_window_layout = [
                                   [sg.Text('RMP', size=(7,1)),sg.Text('[mV]', size = (4,1)),sg.InputText('', size=(6,1), key='RMP')],
                                   [sg.Text('CT', size=(7,1)),sg.Text('[ms]', size = (4,1)),sg.InputText('', size=(6,1), key='CT')],
                                   [sg.Text('Max mV'  , size=(7,1)),sg.Text('[mV]', size = (4,1)),sg.InputText('', size=(6,1), key='maxvolt')],
                                   [sg.Button('Calculate', key ='-MANUAL-CALC-')],
                                   [sg.Text('AMP', size=(7,1)),sg.Text('[mV]', size = (4,1)),sg.InputText(manual_amp, size=(6,1), key='AMP')],
                                   [sg.Text('APD90', size=(7,1)),sg.Text('[ms]', size = (4,1)),sg.InputText('', size=(6,1), key='APD90'),sg.Text(manual_apd90, key='manual_apd90',size = (17,1), justification = 'left')],
                                   [sg.Text('APD75', size=(7,1)),sg.Text('[ms]', size = (4,1)),sg.InputText('', size=(6,1), key='APD75'),sg.Text(manual_apd75, key='manual_apd75',size = (17,1), justification = 'left')],
                                   [sg.Text('APD50', size=(7,1)),sg.Text('[ms]', size = (4,1)),sg.InputText('', size=(6,1), key='APD50'),sg.Text(manual_apd50, key='manual_apd50',size = (17,1), justification = 'left')],
                                   [sg.Text('APD25', size=(7,1)),sg.Text('[ms]', size = (4,1)),sg.InputText('', size=(6,1), key='APD25'),sg.Text(manual_apd25, key='manual_apd25',size = (17,1), justification = 'left')],
                                   [sg.Text('APD10', size=(7,1)),sg.Text('[ms]', size = (4,1)),sg.InputText('', size=(6,1), key='APD10'),sg.Text(manual_apd10, key='manual_apd10',size = (17,1), justification = 'left')],
                                   [sg.Text('Notch', size=(7,1)),sg.Text('[ms]', size = (4,1)),sg.InputText('', size=(6,1), key='NT')],  
                                   [sg.Text('Notch', size=(7,1)),sg.Text('[mV]', size = (4,1)),sg.InputText('', size=(6,1), key='NV')],
                                   [sg.Text('Plateau', size=(7,1)),sg.Text('[ms]', size = (4,1)),sg.InputText('', size=(6,1), key='PT')],
                                   [sg.Text('Plateau', size=(7,1)),sg.Text('[mV]', size = (4,1)),sg.InputText('', size=(6,1), key='PV50')],
                                   [sg.Button('Correct with CT', key ='-MANUAL-CORR-')],
                                   [sg.Button('Accept', key ='-MANUAL-ACCEPT-', button_color=('white', 'green'))],
                                  ]
                manual_parameters_window = sg.Window('Manual analysis',manual_parameters_window_layout,keep_on_top=True,finalize=True)
                manual_parameters_window_active = True        
            
        if manual_parameters_window_active:
            event, values = manual_parameters_window.read(timeout=100)                
            if event == '-MANUAL-CALC-':
                try:
                    if values['RMP'] != '' and values['maxvolt'] != '':
                        manual_amp = round(float(values['maxvolt']) - float(values['RMP']),1)
                        manual_mdp = float(values['RMP'])
                        manual_apd90 = 'APD90 [mV]    ' + str(round(manual_mdp + manual_amp*0.1,1))
                        manual_apd75 = 'APD75 [mV]    ' + str(round(manual_mdp + manual_amp*0.25,1))
                        manual_apd50 = 'APD50 [mV]    ' + str(round(manual_mdp + manual_amp*0.5,1))
                        manual_apd25 = 'APD25 [mV]    ' + str(round(manual_mdp + manual_amp*0.75,1))
                        manual_apd10 = 'APD10 [mV]    ' + str(round(manual_mdp + manual_amp*0.9,1))
                        manual_parameters_window['AMP'].update(manual_amp)
                        manual_parameters_window['manual_apd90'].update(manual_apd90)
                        manual_parameters_window['manual_apd75'].update(manual_apd75)
                        manual_parameters_window['manual_apd50'].update(manual_apd50)
                        manual_parameters_window['manual_apd25'].update(manual_apd25)
                        manual_parameters_window['manual_apd10'].update(manual_apd10)
                    if values['PT'] == '' and values['APD90'] != '':
                        manual_parameters_window['PT'].update(round(float(values['APD90'])/2,1))
                except ValueError:
                    print('Manual analysis textboxes must be filled with numbers.')
            
            if event == '-MANUAL-CORR-':
                if values['CT'] != '':
                    try:
                        for key in ('APD90','APD75','APD50','APD25','APD10','PT','NT'):
                            if values[key] != '':
                                if correction_dict[key] != float(values['CT']):
                                    original_value = float(values[key]) - correction_dict[key]
                                    manual_parameters_window[key].update(round(original_value-float(values['CT']),1))
                                    correction_dict[key] = float(values['CT'])
                    except ValueError:
                        print('Manual analysis textboxes must be filled with numbers.')
                else:
                    print('Please, enter a CT value for correction')
            
            if event == '-MANUAL-ACCEPT-':
                manual_data_temp = {}
                try:
                    for key in ('MEAS','ID','ID_OLD','CL','TRAIN','CT','RMP','AMP','dvmax','dvmin','APD90','APD75','APD50',
                                        'APD25','APD10', 'PV50', 'PV50%', 'MPS', 'TR', 'NdVmin', 'NV', 'NT', 'NV-R', 'NV-R%', 'dvmin_time'):
                        try:
                            if key == 'MEAS' or key == 'CL' or key == 'TRAIN':
                                manual_data_temp[key]=str(manual_data[0][key])                       
                            elif key == 'ID':
                                manual_data_temp[key]=str(manual_data[0]['ID'])+'M'
                            elif key == 'ID_OLD':
                                manual_data_temp[key]=str(manual_data[0]['ID'])
                            elif key == 'dvmax' or key == 'dvmin' or key == 'NdVmin' or key == 'dvmin_time':
                                if isinstance(manual_data[0][key], str) == True:     
                                    manual_data_temp[key]=manual_data[0][key]
                                else:
                                    manual_data_temp[key]=round(manual_data[0][key],1)
                            elif key == 'TR':
                                try:
                                    manual_data_temp[key]=str(round(float(values['APD90'])-float(values['APD25']),1))
                                except ValueError:
                                    manual_data_temp[key]='NA'
                            elif isinstance(values[key], str) == True and values[key] != '':
                                manual_data_temp[key]=values[key]
                            elif values[key] == '':
                                manual_data_temp[key]='NA'
                            else:
                                manual_data_temp[key]=str(round(values[key],1))
                        except KeyError:
                            manual_data_temp[key]='NA'
                except ValueError:
                    print('Manual analysis textboxes must be filled with numbers.')
                manual_data_list.append(manual_data_temp)
                window.write_event_value('-START-', '')
                manual_parameters_window.close()
                manual_parameters_window_active = False

            if event == sg.WIN_CLOSED:
                manual_parameters_window.close()
                manual_parameters_window_active = False
                
        
        elif not average_window_active and event == '-AVERAGE-':    
            average_window_layout = [
                                    [sg.Text('Please, check if the averaged potentials are correct.')],
                                    [sg.Button('Export', key ='-WRITEAVERAGE-', button_color=('white','green')),
                                     sg.Button('Matplotlib', key='-MATPLOTAVERAGE-', button_color=('white', 'MediumPurple2')),
                                     sg.Button('Cancel')]]
            
            if len(values['-TABLE-CLASSIC-']) > 1:
                chosen_table_rows = values['-TABLE-CLASSIC-']
                potentials_to_average = []
                highlighted_rows = []
                averaged_potential = {}
                print('Averaging...')
                try:
                    for e in range(len(chosen_table_rows)):
                        potentials_to_average.append(data_list[chosen_table_rows[e]])
                        print(data_list[chosen_table_rows[e]]['ID'])
                    print()
                    averaged_potential = average_potentials(potentials_to_average)
                except IndexError:
                    print('Press start and select again.')
                    
                if averaged_potential != {}:
                    averaged_potentials_list = []
                    averaged_potentials_list.append(averaged_potential)
                    x, y = create_plot_data(averaged_potentials_list, 'none', 0)
                    if fig_canvas_agg != None:
                        delete_fig_agg(fig_canvas_agg)
                    fig_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas,
                                                 plot_canvas(x, y, color_list, data_list, fig_canvas_dpi, variable_scatter))
                    average_window = sg.Window('Averaging', average_window_layout)
                    average_window_active = True        
                else:
                    print('Averaging failed.')
            else:
                print('Choose at least 2 potentials to average them.')
                    
        if average_window_active:
            from_popup = True
            row_color = 'orange'
            event, values = average_window.read(timeout=100)
            if event != sg.TIMEOUT_KEY:
                pass
                average_window_active = False
                average_window.close()
            
            if event == '-MATPLOTAVERAGE-':
                plot_window(x, y, title, color_list, averaged_potentials_list)
                average_window_active = False
                average_window.close()
                window.write_event_value('-START-', '')
                
            if event == 'Cancel' or event == sg.WIN_CLOSED:
                average_window_active = False
                average_window.close()
                window.write_event_value('-START-', '')
                
        elif not auto_average_window_active and event == '-AUTOAVERAGE-':
            print('Calculating averages...')
            auto_average_window_layout = [
                                        [sg.Text('Please, check if the averaged potentials are correct.')],
                                        [sg.Button('Create', key ='-WRITEAVERAGE-', button_color=('white','green')),
                                         sg.Button('Matplotlib', key='-MATPLOTAVERAGE-', button_color=('white', 'MediumPurple2')),
                                         sg.Button('Cancel')]]
            averaged_potentials_list = auto_average_potentials(data_list)
            if averaged_potentials_list != []:
                x, y = create_plot_data(averaged_potentials_list, 'none', 0)
                if fig_canvas_agg != None:
                    delete_fig_agg(fig_canvas_agg)
                fig_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas,
                                             plot_canvas(x, y, color_list, data_list, fig_canvas_dpi, variable_scatter=[]))
                auto_average_window = sg.Window('Automatic averaging', auto_average_window_layout)
                auto_average_window_active = True
        
        if auto_average_window_active:
            from_popup = True
            event, values = auto_average_window.read(timeout=100)
            if event != sg.TIMEOUT_KEY:
                pass
                auto_average_window_active = False
                auto_average_window.close()
            
            if event == '-MATPLOTAVERAGE-':
                plot_window(x, y, title, color_list, averaged_potentials_list)
                auto_average_window_active = False
                auto_average_window.close()
                window.write_event_value('-START-', '')
                
            if event == 'Cancel' or event == sg.WIN_CLOSED:
                    auto_average_window_active = False
                    auto_average_window.close()
                    window.write_event_value('-START-', '')

        if event == '-WRITEAVERAGE-':
            for potential in range(len(averaged_potentials_list)):
                averaged_potential = export_averages(averaged_potentials_list[potential])
                chosen_files.append(averaged_potential)
            for oldfile in range(len(chosen_files)):
                try:
                    for newfile in range(len(averaged_potentials_list)):
                        if chosen_files[oldfile]['filename'] == averaged_potentials_list[newfile]['filename']:
                            del chosen_files [oldfile]
                except IndexError:
                    pass
                
                for file in range(len(chosen_files)):
                    display_filenames.append(chosen_files[file]['filename'])
                window['-INFILES-'].update(values=display_filenames)
            recalculate = True
            window.write_event_value('-START-', '')
    
        elif event == '-RESET-DB-':
            for var in ['-PNUM0-', '-PNUM1-', '-PNUM2-','-PNUM3-','-PNUM4-',
                        '-PNUM5-', '-PNUM6-',
                        '-DNUM0-','-DNUM1-', '-DNUM2-', '-DNUM3-',
                        '-DNUM4-', '-DNUM5-', '-LASTF-',
                        '-RNUM0-', '-RNUM1-', '-RNUM2-', '-RNUM3-', '-RNUM4-',
                        '-RTIME0-', '-RTIME1-', '-RTIME2-', '-RTIME3-',
                        '-RTIME4-'
                        ]:
                window[var].update('')
            window['-COUNT-'].update(1)
            window['-DB-INFO-'].update('')
                
            if values['-HDATE-'] == False:
                window['-DATE-'].update('')
            
            if values['-HPREP-'] == False:
                window['-PREP-'].update('')
            
            if values['-HDRUGS-'] == False:
                for var in ['-DNAME1-', '-DNAME2-', '-DNAME3-',
                            '-DNAME4-', '-DNAME5-']:
                    window[var].update('')
            
            if values['-HPROT-'] == False:
                for var in ['-PNAME0-', '-PNAME1-', '-PNAME2-', '-PNAME3-',
                        '-PNAME4-', '-PNAME5-', '-PNAME6-']:
                    window[var].update('')
            if values['-HBCL-'] == False:
                window['-BCL-'].update('')
        
        elif event == '-CREATE-DB-' or str(event) == 'Return:36':
            outfile_ready = False
            if values['-DB-OUTFILE-'] == '':
                if values['-COUNT-'] == 1:
                    outfile_path = str(os.getcwd()+'/'+values['-DATE-']+'-'+values['-PREP-']+'-' +values['-DNAME1-'] +'.csv')
                else:
                    outfile_path = str(os.getcwd()+'/'+values['-DATE-']+'-'+values['-COUNT-']+'-'+values['-PREP-']+'-'+values['-DNAME1-'] +'.csv')
                window['-DB-OUTFILE-'].update(outfile_path)
                outfile_mode ='w'
                outfile_ready = True
            
            else:
                outfile_path = values['-DB-OUTFILE-']
                if os.path.exists(outfile_path):
                    if outfile_path[-4:] == '.csv' or outfile_path[-4:] == '.CSV':
                        outfile_mode ='a'
                        outfile_ready = True
            
            if outfile_ready == True:
                window['-DB-INFO-'].update('')
                if values['-DATE-'] != '' and values['-LASTF-'] != '':
                    protocol0=list(values['-PNUM0-'].split(","))
                    protocol1=list(values['-PNUM1-'].split(","))
                    protocol2=list(values['-PNUM2-'].split(","))
                    protocol3=list(values['-PNUM3-'].split(","))
                    protocol4=list(values['-PNUM4-'].split(","))
                    protocol5=list(values['-PNUM5-'].split(","))
                    
                    if values['-DNUM0-'] == '' :
                        values['-DNUM0-'] = values['-LASTF-']
                    if values['-DNUM1-'] == '' :
                        values['-DNUM1-'] = values['-LASTF-']
                    if values['-DNUM2-'] == '' :
                        values['-DNUM2-'] = values['-LASTF-']
                    if values['-DNUM3-'] == '' :
                        values['-DNUM3-'] = values['-LASTF-']
                    if values['-DNUM4-'] == '' :
                        values['-DNUM4-'] = values['-LASTF-']
                    if values['-DNUM5-'] == '' :
                        values['-DNUM5-'] = values['-LASTF-']
                    
                   
                    outfile = open(outfile_path, outfile_mode)
                    
                    for i in range(1,int(values['-LASTF-'])+1):
                        ftype=''
                        protocol=''
                        # check drug effect
                        if i < int(values['-DNUM1-']):
                            ftype=(values['-DNAME0-'])
                            prevt=ftype
                        elif i < int(values['-DNUM2-']):
                            ftype=values['-DNAME1-']
                            prevt=ftype
                        elif i < int(values['-DNUM3-']):
                            ftype=values['-DNAME2-']
                            prevt=ftype
                        elif i < int(values['-DNUM4-']):
                            ftype=values['-DNAME3-']
                            prevt=ftype
                        elif i < int(values['-DNUM5-']):
                            ftype=values['-DNAME4-']
                            prevt=ftype
                        elif i < (int(values['-LASTF-'])):
                            ftype=values['-DNAME5-']
                            prevt=ftype
                        elif i == (int(values['-LASTF-'])):
                            ftype=prevt
                        
                        # check restitution
                        if str(i) == str(values['-RNUM0-']):
                            protocol=','+'REST-'+values['-RTIME0-']
                        elif str(i) == str(values['-RNUM1-']):
                            protocol=','+'REST-'+values['-RTIME1-']
                        elif str(i) == str(values['-RNUM2-']):
                            protocol=','+'REST-'+values['-RTIME2-']
                        elif str(i) == str(values['-RNUM3-']):
                            protocol=','+'REST-'+values['-RTIME3-']
                        elif str(i) == str(values['-RNUM4-']):
                            protocol=','+'REST-'+values['-RTIME4-']
                        
                        # check protocol
                        elif str(i) in protocol0:
                            protocol=','+values['-PNAME0-']
                        elif str(i) in protocol1:
                            protocol=','+values['-PNAME1-']
                        elif str(i) in protocol2:
                            protocol=','+values['-PNAME2-']
                        elif str(i) in protocol3:
                            protocol=','+values['-PNAME3-']
                        elif str(i) in protocol4:
                            protocol=','+values['-PNAME4-']
                        elif str(i) in protocol5:
                            protocol=','+values['-PNAME5-']
                        else:
                            protocol=','+str(values['-BCL-'])
                    
                        if i < 10:
                            outline=(values['-DATE-'] +',"0' + str(i) + '",'+ ftype + protocol)
                        else:
                            outline=(values['-DATE-'] +',' + str(i) + ','+ ftype + protocol)
                        print(outline)
                        outfile.write(outline+'\n')
                    outfile.close()
                    window['-DB-INFO-'].update('Successful ID data generation.')
            else:
                window['-DB-INFO-'].update('Please, select a valid CSV file.')
    
    window.close()

def create_plot_data(data_list, corr_type='none', correct_to=0):

    # This function:
    # 1. generates x and y lists of equal lengths
    #    (otherwise matplotlib will fail to draw the plot);
    # 2. joins these x and y lists as separate lists of x's and y's
    #    as opposed to the pairs of xy's in the case of action potential analysis;
    # 3. applies corrections for RMP or AMP as needed.
    x = []
    y = []
    
    for e in range(len(data_list)):
        # Determining shortest data list for each potential
        length_list = [len(data_list[e]['x']),len(data_list[e]['y'])]
        length_list.sort()
        maximum_length = length_list[0]
        # No correction:        
        if corr_type == 'none':
            x_temp = []
            y_temp = []
            for i in range(maximum_length):
                x_temp.append(data_list[e]['x'][i])
                y_temp.append(data_list[e]['y'][i])          
            x.append(x_temp)
            y.append(y_temp)
        
        # RMP correction:
        elif corr_type == 'mdp':
            try:
                x_temp=[]
                ymdp_temp=[]
                mdp_corr = data_list[correct_to]['RMP']-data_list[e]['RMP']
                # stat_list[num] position could point to any potential though

                # Generating corrected dataset
                for i in range(maximum_length):
                    x_temp.append(data_list[e]['x'][i])
                    ymdp_temp.append(data_list[e]['y'][i] + mdp_corr)
                x.append(x_temp)
                y.append(ymdp_temp)
            
            except:
                print('An error occured, no correction was done.')
                x_temp = []
                y_temp = []
                for i in range(maximum_length):
                    x_temp.append(data_list[e]['x'][i])
                    y_temp.append(data_list[e]['y'][i])
                x.append(x_temp)
                y.append(y_temp)  

       # AMP correction
        elif corr_type == 'amp':
            x_temp=[]
            amp_temp=[]
            yamp_temp=[]
            
            try:
                # Defining amount of correction needed
                amp_corr = (data_list[correct_to]['AMP'] - data_list[e]['AMP']) / data_list[e]['AMP']
                
                # Generating AMP corrected dataset
                for i in range(len(data_list[e]['x'])):
                    amp_temp.append(data_list[e]['y'][i] + data_list[e]['y'][i] * amp_corr)
                    
                # Getting RMP for AMP-corrected data
                sum_mdp = 0
                counter = 0
                for line in range(maximum_length-25,
                                  maximum_length-5):
                    sum_mdp=sum_mdp+(amp_temp[line])
                    counter=counter+1

                # Applying RMP correction to AMP corrected data
                mdp_temp = sum_mdp/counter
                mdp_corr_amp = data_list[0]['RMP'] - mdp_temp

                # Generating datalist
                for i in range(maximum_length):
                    x_temp.append(data_list[e]['x'][i])
                    yamp_temp.append(amp_temp[i] + mdp_corr_amp)
                x.append(x_temp)
                y.append(yamp_temp)
            
            except ZeroDivisionError:
                print('Amplitude correction is not possible due to 0 mV AMP value.')
                print('No correction is done.')
                x.append(data_list[e]['x'])
                y.append(data_list[e]['y'])
                
            except:
                print('Amplitude correction is not possible.')
                print('No correction is done.')
                x.append(data_list[e]['x'])
                y.append(data_list[e]['y'])
    
    # First derivative to figure
        elif corr_type == 'fdv':
            x_temp = []
            y_temp = []
            for i in range(maximum_length):
                x_temp.append(data_list[e]['x'][i])
                y_temp.append(data_list[e]['fdv'][i])
            x.append(x_temp)
            y.append(y_temp)
        
        elif corr_type == 'repol':
            x_temp = []
            y_temp = []
            for i in range(data_list[e]['maxvolt_line'], maximum_length):
                new_y = -100*(1-((data_list[e]['y'][i]-data_list[e]['RMP'])/data_list[e]['AMP']))
                if new_y > -95:
                    x_temp.append(new_y)
                    y_temp.append(data_list[e]['x'][i])
            x.append(x_temp)
            y.append(y_temp)

    return x,y

def variable_to_scatter(value, value_axis, other_axis, x_value_given):
    maxlines = len(value_axis)-1
    if x_value_given:
        for line in range(maxlines, 0, -1): # going backwards to avoid matches from phase 0
            try:
                if value_axis[line] <= value:
                        return value_axis[line], other_axis[line]
            except: # e. g., value is "NA"
                log(str('variable_to_scatter failed on x (time) value'))
                return 0, -110
    else:
        for line in range(maxlines, 0, -1): # going backwards to avoid matches from phase 0
            try:
                if value_axis[line] >= value:
                        return other_axis[line], value_axis[line]
            except: # e. g., value is "NA"
                log(str('variable_to_scatter failed on y (voltage) value'))
                return 0, -110

def plot_empty_figure(fig_canvas_dpi):
    fig = matplotlib.figure.Figure(figsize=(7, 6), dpi=fig_canvas_dpi)
    ax=fig.add_subplot(111)
    ax.plot([0,500],[-100,60], color='white')
    ax.set(xlabel='Time (ms)', ylabel='Potential (mV)')
    return fig

def delete_fig_agg(fig_canvas_agg):
    fig_canvas_agg.get_tk_widget().forget()
    plt.close('all')

def draw_figure(canvas, figure):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=0)
    return figure_canvas_agg

def plot_canvas(x, y, color_list, data_list, fig_canvas_dpi, variable_scatter=[]):
    fig = matplotlib.figure.Figure(figsize=(7, 6), dpi=fig_canvas_dpi)
    ax = fig.add_subplot(111)
    if color_list == []:
        for e in range(len(x)):
            try:
                ax.plot(x[e],
                        y[e]
                        )
                if variable_scatter != []:
                    ax.scatter(variable_scatter[e]['x'], variable_scatter[e]['y'])
            except:
                log(str('plot_canvas failed without color_list'))

    else:
        for e in range(len(x)):
            try:
                ax.plot(x[e],
                        y[e],
                        color=color_list[e])
                if variable_scatter != []:
                    ax.scatter(variable_scatter[e]['x'], variable_scatter[e]['y'])
            except:
                log(str('plot_canvas failed with color_list'))

    ax.set(xlabel='Time (ms)', ylabel='Potential (mV)')
    #ax.set_xlim(0,500)
    ax.set_ylim(-100,80)
    return fig

def plot_window(x, y, title, color_list, data_list, variable_scatter):
    figtitle = title
    fig, ax = plt.subplots()
    filename_old = ""
    if color_list == []:
        for e in range(len(x)):
            try:
                if variable_scatter != []:
                    ax.scatter(variable_scatter[e]['x'], variable_scatter[e]['y'])
                ax.plot(x[e],
                        y[e],
                        label=data_list[e]['ID']
                        )
            except:
                log(str('plot_window failed without color_list'))
    else:
        for e in range(len(x)):
            try:
                if variable_scatter != []:
                    #ax.scatter(variable_scatter[e][0], variable_scatter[e][1])
                    ax.annotate(variable_scatter[e]['name']+' '+str(variable_scatter[e]['value']), xy=(variable_scatter[e]['x'],variable_scatter[e]['y']), xytext=(-40,40),textcoords="offset points",
                        bbox=dict(boxstyle='round4', fc='linen',ec='k',lw=1),arrowprops=dict(arrowstyle='-|>'))
                ax.plot(x[e],
                        y[e],
                        label=data_list[e]['ID'],
                        color=color_list[e])
            except:
                log(str('plot_window failed with color_list'))
    ax.set(xlabel='Time (ms)', ylabel='Potential (mV)',
           title = figtitle)
    ax.legend()
    #ax.set_xlim(0,500)
    ax.set_ylim(-100,80)
    
    cursor = Cursor(ax, horizOn=True, vertOn=True, useblit=True,
                color = 'black', linewidth = 1)
    
    # Creating an annotating box
    annot_abs = ax.annotate("", xy=(0,0), xytext=(-40,40),textcoords="offset points",
                        bbox=dict(boxstyle='round4', fc='linen',ec='k',lw=1),
                        arrowprops=dict(arrowstyle='-|>'))
    annot_rel = ax.annotate("", xy=(0,0), xytext=(-40,40),textcoords="offset points",
                        bbox=dict(boxstyle='round4', fc='linen',ec='k',lw=1),
                        arrowprops=dict(arrowstyle='-|>'))
    annot_abs.set_visible(False)
    annot_rel.set_visible(False)
    
    global abs_coord
    abs_coord = []
    
    def onclick(event):
        #rel_coord.append((event.xdata, event.ydata))
        x = event.xdata
        y = event.ydata
        annot_rel.xy=(x,y)
        if abs_coord == []:
            text = "Absolute values:\n" + str(round(x,1))+" ms; "+str(round(y,1))+" mV"
        else:
            x_rel = x - abs_coord[0][0]
            y_rel = y - abs_coord[0][1]
            text = "Relative values:\n" + str(round(x_rel,1))+" ms; "+str(round(y_rel,1))+" mV"
        annot_rel.set_text(text)
        annot_rel.set_visible(True)
        fig.canvas.draw() #redraw the figure
    
    def onkeypress(event):
        global abs_coord
        abs_coord = [(event.xdata, event.ydata)]
        x = event.xdata
        y = event.ydata
        annot_abs.xy = (x,y)
        text = "Fixed values:\n" + str(round(x,1))+" ms; "+str(round(y,1))+" mV"
        annot_abs.set_text(text)
        annot_abs.set_visible(True)
        fig.canvas.draw()
    
    def onscroll(event):
        global abs_coord
        abs_coord = []
        annot_abs.set_visible(False)
        fig.canvas.draw()
        
    fig.canvas.mpl_connect('button_press_event', onclick)
    fig.canvas.mpl_connect('key_press_event', onkeypress)
    fig.canvas.mpl_connect('scroll_event', onscroll)
    plt.show()

def export_stats(data_list, outfile_name):
    outfile = open(outfile_name, 'w')
    outfile.write('MEAS,ACQ,TRAIN,CL,CT,RMP,AMP,dvmax,dvmin,APD90,APD75,APD50,APD25,APD10,dvmin_time,OSV,OST,PV20,PV20%,PV50,PV50%,PV75,PV75%,MPS,TR,NdVmin,NV,NT,NV-R,NV-R%,MDP,TOPV,TOPT,EDD,LDD,RMP,AMP,APD\n')
    for element in range(len(data_list)):
        newline = ''
        
        for key in ('MEAS','ACQ','TRAIN','CL','CT','RMP','AMP','dvmax','dvmin','APD90','APD75','APD50',
                                'APD25','APD10','dvmin_time','OSV','OST','PV20', 'PV20%','PV50', 'PV50%','PV75', 'PV75%', 'MPS', 'TR', 'NdVmin', 'NV', 'NT', 'NV-R', 'NV-R%','MDP', 'TOPV', 'TOPT', 'EDD', 'LDD', 'RMP', 'AMP', 'APD'):
                try:
                    if isinstance(data_list[element][key], str) == True:
                        newline = newline +str(data_list[element][key]) + ','
                    else:
                        newline = newline +str(round(data_list[element][key],1)) + ','
                except KeyError:
                    newline = newline + 'NA' + ','
        newline=newline+'\n' 
        outfile.write(newline)
    outfile.close()
    print('Exported as: ' + outfile_name)
    
def export_potentials(number_of_potentials, t, ycorr, data_list, outfile_name):
    maxlines=0
    for potential in range(number_of_potentials):
        currentlines=len(t[potential])
        if len(t[potential]) > maxlines:
            maxlines=len(t[potential]) 
    outfile = open(outfile_name, 'w')
    
    for line in range(maxlines):
        if line == 0: # header
            for potential in range (number_of_potentials):
                outfile.write(str(data_list[potential]['ID']) + "_ms," +
                              str(data_list[potential]['ID']) + "_mv," )
            outfile.write('\n')
            
        for potential in range (number_of_potentials):
            if line < len(t[potential]):
                outfile.write(
                     str(t[potential][line]) + ',' +
                     str(ycorr[potential][line]) + ',')
            else:
                outfile.write(
                     '""' + ',' +
                     '""' + ',')
        outfile.write('\n')
    outfile.close()
    print('Exported as: '+ outfile_name)
    
def create_filename(original_id):
    split_id = (str(original_id)).split('.')
    outfile_name = ''
    for segment in range(len(split_id)-1):
        if segment > 0 :
            outfile_name = outfile_name + '_'
        outfile_name = outfile_name + split_id[segment]
    return outfile_name    
    
def average_potentials(potentials_to_average):
    return_potential = {}
    return_potential['filename'] = potentials_to_average[0]['filename']
    return_potential['filedir'] = potentials_to_average[0]['filedir']
    return_potential['wd'] = potentials_to_average[0]['wd']
    return_potential['x'] = []
    length_list = []
    time_axis = []
    averaged_potential = []
    outfile_name = create_filename(potentials_to_average[0]['filename'])
    
    for potential in range(len(potentials_to_average)):    
        length_list.append(len(potentials_to_average[potential]['x']))
        length_list.append(len(potentials_to_average[potential]['y']))
    length_list.sort()
    maximum_length = length_list[0]
    sum_potential = []
    
    for line in range(maximum_length):
        sum_potential.append(0)
        
    for potential in range(len(potentials_to_average)):
        for line in range(maximum_length):
            sum_potential[line] = sum_potential[line] + potentials_to_average[potential]['y'][line]
    
    for line in range(maximum_length):
        averaged_potential.append(sum_potential[line]/len(potentials_to_average))
        return_potential['x'].append(potentials_to_average[0]['x'][line])    
      
    if averaged_potential != []:
        return_potential['y'] = averaged_potential
        return_potential['ID'] = outfile_name
        return_potential['TRAIN'] = potentials_to_average[0]['TRAIN']
        return_potential['CL'] = potentials_to_average[0]['CL']
        
    return return_potential

def auto_average_potentials(data_list):
    averaged_potentials_list = []
    file_list = []
    id_list = []
    oldfile = ""
    check_cl = True #should be made accessible through a checkbox
    cl_block = False
    
    # Identifying individual files from all processed potentials
    for potential in range(len(data_list)):
        if data_list[potential]['filename'] != oldfile:
            file_list.append(data_list[potential]['filename'])
            oldfile = data_list[potential]['filename']
    averaged_potentials = []

    # Processing each file individually
    for file in range(len(file_list)):
        new_potential = {}
        new_potential['TRAIN'] = ''
        potential_lengths = []
        this_potential = []
        sum_potential = []
        all_potentials_infile = []
        time_axis = []
        cl_list = []
        
        # Finding all potentials in the file
        for potential in range(len(data_list)):
            if data_list[potential]['filename'] == file_list[file]:
                all_potentials_infile.append(data_list[potential]['y'])
                potential_lengths.append(len(data_list[potential]['x']))
                potential_lengths.append(len(data_list[potential]['y']))
                cl_list.append(data_list[potential]['CL'])
                if new_potential['TRAIN'] == '':
                    new_potential['TRAIN'] = data_list[potential]['TRAIN']
                    new_potential['CL'] = data_list[potential]['CL']
                    new_potential['filename'] = data_list[potential]['filename']
                    new_potential['filedir'] = data_list[potential]['filedir']
                    new_potential['wd'] = data_list[potential]['wd']
                if time_axis == []:
                    time_axis = data_list[potential]['x']
               
        if check_cl == True:
            for potential in range(len(cl_list)):
                if cl_list[potential] != new_potential['CL']:
                    all_potentials_infile = []
                    cl_block = True
                    break
            
        if len(all_potentials_infile) > 2:             
            potential_lengths.sort()
            maximum_length = potential_lengths[0]
            
            # Creating an averaged potential from *all* potentials
            for line in range(maximum_length):
                    sum_potential.append(0)
            
            for potential in range(len(all_potentials_infile)):        
                for line in range(maximum_length):
                    sum_potential[line] = sum_potential[line] + all_potentials_infile[potential][line]

            raw_averaged_potential = []
            
            for line in range(maximum_length):
                raw_averaged_potential.append(sum_potential[line]/len(all_potentials_infile))
                
            # Choosing potentials that are similar to the average
            good_potentials = []
            for potential in range(len(all_potentials_infile)):
                difference = False
                for line in range(len(raw_averaged_potential)-1):
                    testline = all_potentials_infile[potential][line]
                    if difference == False:
                        if time_axis[line] > 20: # ignoring the first 20 ms time to skip pacing artefacts
                            if raw_averaged_potential[line] > testline+5 or raw_averaged_potential[line] < testline-5:
                                difference = True
                    if difference == False and line == maximum_length-10:
                        good_potentials.append(all_potentials_infile[potential])
            
            if good_potentials != []:
                potentials_to_average = []
                for potential in range(len(good_potentials)):
                    new_potential['x'] = time_axis
                    new_potential['y'] = good_potentials[potential]
                    potentials_to_average.append(new_potential)
                averaged_potential = average_potentials(potentials_to_average)
                if averaged_potential != []:
                    averaged_potentials_list.append(averaged_potential)

            
            else:
                print('Automatic averaging of',new_potential['filename'],'failed, select potentials to average manually.')
        
        elif cl_block == True:
            print(new_potential['filename'],'was skipped due to different cycle lengths.')
        
        else:   
            print('Not enough potentials in file',new_potential['filename'])
    
    return averaged_potentials_list

def export_averages(averaged_potential):
    outfile_name = averaged_potential['ID'] + 'A.csv'
    outfile_path = averaged_potential['filedir'] + '/'+ averaged_potential['ID'] + 'A.csv'
    try:
        outfile = open(outfile_path, 'w')
        for line in range(len(averaged_potential['x'])):
            if line == 0: # header
                outfile.write('ACQ,TRAIN\n' )

            elif line == 1: # header
                outfile.write(outfile_name+',' + averaged_potential['TRAIN']+'.TRA'+'\n')
            
            elif line == 2:
                try:
                    outfile.write('Cycle,'+ averaged_potential['CL']+'\n')
                except:
                    outfile.write('Cycle,'+'NA'+'\n')
            
            elif line < len(averaged_potential['x']):
                outfile.write(
                     str(averaged_potential['x'][line]) + ',' +
                     str(averaged_potential['y'][line]) + ',')
                outfile.write('\n')
        outfile.close()
        print('Exported as: '+ outfile_name)
        return {'filename': averaged_potential['ID'] + 'A.csv', 'wd': averaged_potential['wd']}
    
    except:
        return outfile_name
        print('An error has occured, no export was made.')
                
def colorgen(argn):
    # Generate Random Colors Programmatically
    # https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/
    def hsv_to_rgb(h, s, v):
        if s == 0.0: return (v, v, v)
        i = int(h*6.) # XXX assume int() truncates!
        f = (h*6.)-i; p,q,t = v*(1.-s), v*(1.-s*f), v*(1.-s*(1.-f)); i%=6
        if i == 0: return (v, t, p)
        if i == 1: return (q, v, p)
        if i == 2: return (p, v, t)
        if i == 3: return (p, q, v)
        if i == 4: return (t, p, v)
        if i == 5: return (v, p, q)

    golden_ratio_conjugate = 0.618033988749895
    color_list = []
    random.seed(2)
    for i in range(argn):    
        h = random.uniform(0,1)# use random start value
        h = h + golden_ratio_conjugate
        h %= 1
        color_list.append(hsv_to_rgb(h, 0.6, 0.9))
    del color_list [0]
    return(color_list)

if __name__ == '__main__':
    main()
