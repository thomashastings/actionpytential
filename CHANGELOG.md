# CHANGELOG #
## v0.8.3 – 2022–03–29 ##
- Improved the cursor experience in the `Matplotlib` window:
    * Lines aid in cursor positioning
    * A reference point can be added
    * If a reference point is present, cursor values are relative distances from the reference point
- Otherwise silent exceptions now show a message in debug mode
- Minor bugfixes

## v0.8.2 – 2022–03–08 ##
- Added a segmentation mode specific to continous recordings from SAN cells (slow response AP), and improved segmentation in general
- Added variables specific to SAN cells (based on Bucchi et al., 2007; doi: 10.1016/j.yjmcc.2007.04.017)
- The table was split into three subtables:
    * Classic: basic variables (CT, AMP, dVmax, APDs)
    * Plateau/Notch: variables related to the plateau and the notch
    * Slow response: variables for slow response-type action potentials
- Added option to plot a chosen variable so it can be visualized on the APs
- Functions related to importing files and analysing individual APs were moved to their corresponding scripts, `import_files.py` and `ap_parameters.py` respectively
- Removed `force_lowres` option from the configuration file (`compact_view` forces low resolution mode as well)
- Minor bugfixes
    
## v0.8.1 – 2022–03–01 ##
- Added compact mode (can be enabled in `config.yml`) for low-res screens
- Minor bugfixes

## v0.8.0 – 2021–12–17 ##
- Added a new mode for AP analysis under the `Resting potential` option, called `Voltage clamp`. This calculates `AMP`, `CT` and `dVmax` values slightly differently to exclude the stimulation artifacts introduced in this measurement type. The default (and recommended) `Resting potential` mode remaines `End`.
- `AMP` is now recalculated if its voltage occurs more than 10 ms later than the timepoint of `dVmax`, as the amplitude of the AP is measured at the end of Phase 0. However, in some cases the highest voltage is measured at the plateau phase (e. g., due to ICa-L activation), leading to false values. Therefore, if the recalculation is triggered by a late `AMP`, the new value for `AMP` will be the highest voltage reached in the 10 ms time period after `dVmax` occured. 
- Arial (or the system equivalent sans serif font) is specified for every text component. This fixes an issue on some Linux-based systems where the automatically chosen font would mess with the window size.
- Added a completely new feature: now you can create your own ID database inside ActionPytential in the second tab of the window.
- Added an experimental config file to force compact mode and dark mode (both are off by default). If the config file is missing, the normal defaults will be used.
- Averaged files now retain the protocol/train name from the original file, if it was present.
- Minor bugfixes

## v0.7.9 – 2021–10–04 ##
- Added two new parameters related to overshoot (voltage above 0 mV): the maximum voltage of the overshoot and the time spent above 0 mV.
- Changed color creation again... Really fast, always enabled. (https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/)
- Benchmark mode can be enabled on its own as a `benchlog` function, not affecting other calls to the `log` function
- Minor bugfixes

## v0.7.8 – 2021–09–08 ##
- The option to choose units of time and voltage of the data was added
- The Gaussian filter was entirely replaced by a low-pass Butterworth filter that is more applicable for the purpose
    * As no feature direclty requires this filter, it will be simply disabled if `scipy` is not available
- Complete recalculation of the action potential data is only done if needed, but it is skipped if the data already in memory can be used (speedup)
- Improvements in manual analysis:
    * The row of the manually analyzed potential will replace the originally selected row in the table instead of appearing at the bottom 
    * The manual analysis window now uses the default font instead of a monospace one

## v0.7.7 – 2021–09–02 ##
- Gaussian-filter based method for identifying action potentials with notch configuration was replaced with a new method 
    * It is based on finding an upstroke in the first derivative of a high-degree (1–30) polynomial function fitted on the first third of the repolarization
    * The accuracy of correctly categorizing notched potentials was increased to 94% from 58% based on 1200 potentials
- The `Cursor` button and plot type was replaced with a new feature called `Manual analysis`
    * This enables computer-aided manual analysis of all parameters otherwise calculated by ActionPytential

## v0.7.6 – 2021–08–20 ##
- Separation of potentials in continuous recordings, and dynamic color creation above 100 potentials can both be turned off when not needed, leading to a noticable speedup, especially on slower systems
- Minor bugfixes:
    * Train names were not showing up properly
    * Folders were showing up in the working directory
    * Window resizing is now disabled in lower resolution mode (to avoid hiding GUI elements)
- Enabled debugging mode: a global `DEBUG` variable should be set to `True`, then the `log()` function can be used to display debug messages, for now, this only includes timings for benchmarking 
    

## v0.7.5 – 2021–08–17 ##
- New variables are calculated: `dVmin` and `dVmin_time`, representing the level and timing of the highest rate of repolarization
- New table functions:
    * Selecting one line: a single potential can be plotted when the corresponding option is chosen
    * Selected lines remain highlighted for the next run
    * Column widths correspond to the length of the expected content 
- From now on, the working directory can be changed at any time, the location of the selected files will be remembered.
    * Note: averaged files are exported to the location of the original file; tables and plot data are exported to the chosen working directory at the time of the export

## v0.7.4 – 2021–08–12 ##
- Beat separation works as intended: every action potential is analyzed individually, but the recordings can be viewed either in full length or in stacked (superposed) view
- Added automatic averaging, only the potentials that are close to the overall average within each file are used (additional options for this TBD)
- Added a preview in the main window for both manual and automatic averaging, as well as a pop-up that offers exporting and a matplotlib preview
- A very basic moving average can replace gaussian_filter1d from scipy if needed, improves support on ARM devices, namely Linux distributions in Termux (Android)
- Enabled window resizing, may help on low resolution displays

## v0.7.3 – 2021–07–30 ##
- Added support for automatic separation of multiple action potentials in a single column/timeline, based on the combination of depolarization rate and absolute voltage
- GUI elements related to beat separation are placeholders for now
- First derivatives of the potentials were also added. They can be viewed, but no variables are derived from them yet

## v0.7.2 – 2021–04–18 ##
- All global variables have been removed
- Multiple action potentials are supported from one file (with separate columns for each potential)
- New table functions:
    * Selecting one line: all corrections will be done to the values of that potential
    * Selecting multiple lines:
        * only selected potentials are plotted, if `Start` is pressed
        * the average of the selected files is written into a new file, and this file is added as a selected file, if `Average` is pressed
- The export option now opens a pop-up with suggested filenames that are editable by the user
- Added a regular expression textbox that can be used to filter from the working directory to add, and from the selected files to remove files based on searh queries
- A commandline window is added with a greeting message. The state of the calculations is also displayed here
- Gaussian smoothing is now available for all plots, and the strength can be selected from the main window
- Fixed issue with some potentials not being properly AMP corrected

## v0.7.1 – 2021–04–18 ##
- Added support for semicolon-delimited files and decimal commas
- Removed a number of global variables (2 remain still)
- Added ID generation from filename for every file
- Comments in action_potential_parameters() now explain all calculated parameters
- Simplified method for correction and plot creation

## v0.7.0 – 2021-04-12 ##
- Tested and working installation procedures via git and PiPy Testing
- Windows and MacOS binaries updated

## v0.6.71 – 2021-04-12 ##
- Created a pipy-testing repository for the project. It might contain broken versions, git version recommended for now.
- Dynamic window sizing has been added for screens with vertical resolutions under 1080 pixels (720 pixels or higher still recommended)
- Tab delimited files are now supported
- Absolute notch time position (NT) was removed, because the notch position can be interpreted only in context of the CT
- Added relative notch % (NV-R%), expressing the depth of the notch as AMP%
- Fixed NVmax being hidden when no notch was found, now NVmax is always shown
 

## v0.6.1 – 2021-04-09 ##
- Files in comma and space separated formats are now supported much better:
    * Separator is chosen dynamically, no user input needed
    * Files are treated individually, thus differently formatted files can be read in the same run
- Enabled exporting of plots in CSV format (rows are `time1,voltage1,time2,voltage2,...`)
- Fixed bug in file listing (files not available in the working directory were showing up)
- Database matching is improved, incorrectly format database will be skipped


## v0.6.0 – 2021-03-31 ##
- All plots are previewed within the main window, with pop-up views available by their respective buttons:
    * Matplotlib: default window provided by Matplotlib. Saving and zooming are available.
    * Cursor: the same window, but 0 mV for the y axis is the MDP, and 0 ms for the x axis is the CT. Therefore, all values are relative to the action potential itself in this mode. 
- Input files are now accessed and converted to `float` only once per run, and they are passed on as `list` variables
- Additional calculated variables concerning the action potential notch:
    * NVmax (maximal rate of early repolarization)
    * NV (notch voltage), the absolute value of the notch-point
    * NT (notch time), the absolute time of the notch-point
    * NV-R (relative notch voltage), delta of the highest voltage (AMP) and the notch point voltage
    * NT-R (notch time), NT corrected for CT


## v0.55 – 2021-01-18 ##
- Additional calculated variables:
    * PP (plateau potential): voltage at the APD90/2 point
    * PP%: the PP expressed as % of action potential amplitude
    * TR (triangulation): APD25 subtracted from APD90
- Added option to define the MDP as:
    * only the end part of the recording (default)
    * only a timepoint before the action potential
    * the average of the above two options
- Minor fixes in file identification

## v0.54 – 2021-01-08 ##
- Added generic CSV mode, files with rows formatted as `time, voltage` are supported
- Added option to ignore the early and late parts of the recording (defined in ms), thus skipping possible artefacts

## v0.54-n – 2021-01-18 ##
- Additional calculated variables:
    * PP (plateau potential): voltage at the APD90/2 point
    * PP%: the PP expressed as % of action potential amplitude
    * TR (triangulation): APD25 subtracted from APD90
- Added option to define the MDP as:
    * only the end part of the recording (default)
    * only a timepoint before the action potential
    * the average of the above two options
- Minor fixes in file identification

## v0.51 – 2021-01-04 ##
- Added GUI using the PySimpleGUI library with the following functionality:
    * Choose working directory
    * Choose database for file identification
    * Choose input files
    * Display action potential parameters in a table
    * Choose plot mode (skip, no corrections, MDP correction, AMP correction)
    * Export button for the table to CSV function
    * Reset button to go back to initial settings 

## alpha1 – 2020-07-15 ##
- Added dynamic color generation if more than 5 potentials are plotted (original code by Andreas Dewes)
- Exported parameters are rounded to 1 decimal

## alpha0 – 2020-06-23 ##
Initial release with the following funcionality:

- Process files conatining action potentials chosen as arguments (only ASCII files exported from APES are supported)
- Add further information about the file by matching it with entries from an external CSV database (optional)
- Find the following parameters of the action potentials:
    * MDP (mean diastolic potential), isoelectric segment at the end of the recording
    * AMP (amplitude), expressed as the delta between the MDP and the highest voltage in the recording
    * CT (conduction time), time from the beginning of the recording to the AMP/2 point
    * Vmax (maximal rate of depolarization)
    * APD90–10 (action potential duration at a given % of repolarization)
- Export parameters to a CSV
- Show the action potentials in a Matplotlib pop-up window
- Apply corrections for changes in MDP and AMP and show those as pop-up windows as well with rows formatted as `time, voltage` are supported
- Added option to ignore the early and late parts of the recording (defined in ms), thus skipping possible artefacts