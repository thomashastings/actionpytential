# Moved to GitLab
The project has been moved to a [new repository](https://gitlab.com/ThomasHastings/ActionPytential), this version will not receive further updates.

# What is ActionPytential? #

ActionPytential is tool for analyzing and visualizing cardiac action potentials, written in Python 3.

The aim of the project is to provide a simple and accessible tool for any researcher to analyze and compare action potential data recorded using their technique and equipment of choice (e. g., conventional microeletrode technique, single cell potential clamp, or *in silico* models). Files from any source can be analyzed, and therefore compared using the same, validated calculations, if they are formatted as ASCII-text.


# Features #

* Analysis for cardiac action potentials (mean diastolic potential, conduction time, amplitude, maximum rate of depolarization, APD values). Calculated values are always provided from the raw data, without any corrections.
* Creation of action potential plots with any number of action potentials.
* Visual corrections for changes in measurements (equalizing mean diastolic potentials and/or amplitudes for all potentials).
* Average action potentials to reduce noise, either by selecting them manually or automatically on a file-by-file basis
* Export all generated data to CSV files, and save all plots as image files.

![Preview](https://bitbucket.org/thomashastings/actionpytential/downloads/ActionPytential_preview.gif)


# Getting Started with ActionPytential #

## 1. Installation ##

### Binaries ###

Binary executables containing all dependencies can be downloaded and run right away. Therefore, they are fairly easy to begin with, but tend to take a bit longer to start up. Such releases are provided for Windows and Mac platforms at [Downloads](https://bitbucket.org/thomashastings/actionpytential/downloads/).


### BitBucket repository (recommended) ###

The recommended way for now is to run the version from BitBucket. This is always updated to the latest stable version.

`python3 -m pip install setuptools sdist`

`git clone https://thomashastings@bitbucket.org/thomashastings/actionpytential.git`

`cd actionpytential`

`python3 setup.py sdist bdist_wheel`

`python3 -m pip install .`

Start the application by running:

`python3 -m ActionPytential`

Linux users may need to install the `python3` and the `python3-tk` or `python3-tkinter` packages.

MacOS users may need to `brew install` the `python3 python-tk` packages.

### PyPi Testing ###

Installation with pip is also possible (for the time being, only from PyPi Testing).

`python3 -m pip install -i --upgrade https://test.pypi.org/simple/ ActionPytential`

Start the application by running:

`python3 -m ActionPytential`

Linux users may need to install the `python3 python3-tk` packages.

MacOS users may need to `brew install` the `python3 python-tk` packages.

## 2. Analyzing your first action potentials ##

Once installed, make sure that you can run ActionPytential by starting the application and confirming that the main window shows up.

A set of sample action potentials (APs) are provided [here](https://bitbucket.org/thomashastings/actionpytential/downloads/ActionPytential_sampledata.zip). Download the archive, and unpack it to a location easy to find, e. g., your Downloads folder.

At the top of the main window of ActionPytential, you should see two lines pointing to the current directory. Click the `Browse` button located at the end of the top line. Navigate to the location where you unpacked the sample data, and choose that directory. Now press the `Update` button just next to `Browse`.

Notice that the sample files showed up on the left pane situated in the main window. Double click the `191127.T13` and `191127.T18` lines, which correspond to the two sample files with the same names. When double-clicking a file, it is chosen for processing and shows up in the pane on the right.

Now, hit the green-colored `Start` button on the bottom. The APs in the files will be processed, and their characteristics will appear in the table, in middle of the window.

Notice the `Plot settings` box next to the pane containing the selected files. In this box, choose the `No corrections` option, and hit the `Start` button again. This time, the APs will appear in the plot area on the right of the window.

Next, choose the `MDP correction` option in the `Plot settings` box, and hit the `Start` button. In this plot, the mean diastolic potentials will be matched, but the numeric data in the table remains unchanged. When you choose `AMP correction`, the amplitudes and the mean diastolic potentials will both be matched between the potentials, but the table will always represent the original data.


# Detailed guide #

### 1. Supported files ###

ASCII text files containing APs are supported when formatted as follows:

`identifier (header)`

`time1, voltage1-1, voltage2-1, voltageN-1`

`time2, voltage1-2, voltage2-2, voltageN-2`

`time3, voltage1-3, voltage2-3, voltageN-3`

`timeM, voltage1-M, voltage2-M, voltageN-M`

The first column is treated as the time axis, all subsequent columns are treated as voltage axes. If a voltage column contains multiple actions potentials (continuous recording), the potentials will be separated and analyzed individually, but can be viewed either as the original continuous recording or stacked (superposed) over each other.

Accepted delimiters are comma (`,`), space (`\s`), semicolon (`;`) and tab (`\t`). Unsupported files will be skipped in most cases. Files with decimal commas instead of decimal points are supported as well. The header should be no longer than 5 lines.

The units of measurement for the files can be chosen by the user. Available units of time are: μs, ms, s; available units of voltage are: pV, nV, μV, mV, V. The list of supported units may be expanded upon request.

### 2. Opening files ###

The program automatically opens the location it is running from and lists all available files, but you can browse to any directory you want, and the files there will be listed after pressing "Update." Double clicking in the left pane moves files to the right pane. Files in the right pane are chosen for processing. Files from there can be removed also by double clicking.

The third input line, `Filter expression`, is a search tool for choosing multiple files faster. The query is parsed as a regular expression. When the `Add` button is pressed, all matching filenames from the working directory will be *added* to the chosen list. When the `Remove` button is pressed, matching files will be *removed* from the list of chosen files. The default query, `.*` will match all files.

### 3. Text-based feedback ###

A textbox is located in the top-right corner that serves as a guide for new users. It also provides feedback on the states of the application, including file operations and exports.

### 4. Continuous recordings ###

Continuous recordings are separated into individual potentials and analyzed as such. Slow response APs (spontaneous activity of the sinoatrial node and of Purkinje fibers) are separated at their minimum diastolic potentials, other AP types are separated based on their amplitudes. Separation mode is set based on the `Resting potential` mode selection.

### 5. Analysis ###

Unwanted high-frequency noise can be filtered out using a low-pass Butterworth filter. The the cutoff frequency can be selected with a slider, the sampling rate is determined automatically for each file.

The analysis provides the following characteristics of each action potential:

- conduction time (CT): the time it takes to reach the midpoint of the upstorke from 0 milliseconds

- resting potential (RP): the isoelectric line after the potential, or the beginning of the potential, or the average of the two, depending on the choice of the user*

- amplitude (AMP): the highest positive point of the potential**

- maximal rate of depolarization, maximal value of dV/dt, displayed as dVmax

- action potential durations (APD10–90) at 10%, 25%, 50%, 75% and 90% repolarization

- plateau-related parameters:
    * PP20-50-90 (plateau potential): voltage at the 20%, 50%, and 90% points of APD90
    * PP%: the PP20-50-90 values expressed as % of action potential amplitude
    * TR (triangulation): APD25 subtracted from APD90
	* MPS (mid-plateau slope): change of voltage in the -50 ms and +50 ms range from the APD90/2 point

- notch-related parameters:
    * NdVmax (maximal rate of early repolarization)
    * NV (notch voltage), the absolute value of voltage at the notch-point***
    * NT (notch time), the time it takes to reach the bottom of the notch from the AMP point***
    * NV-R (relative notch voltage), delta of the highest voltage (AMP) and the notch point voltage***
    * NV-R% (relative notch %), expressing the depth of the notch as AMP%***

- slow response AP parameters (based on [Bucchi et al., 2007](https://pubmed.ncbi.nlm.nih.gov/17543331/)):
    * CL (cycle length, time from the previous beat, as a distance between upstrokes)
	* MDP (minimal diastolic potential)
	* TOPV and TOPT (take off time and voltage at 0.5 mV/ms value of the voltage derivative)
    * EDD (slope of early diastolic depolarization in mV/ms)
	* APD (action potential duration defined as the time from TOPT to the following MDP)
	
The resting potential can be defined in a number of ways, depending on the tissue type and the measurement technique. The simplest and least error-prone method is considering the `End` (average of the last few milliseconds) of each AP. Nevertheless, the first point of the AP can be taken as well (`Beginning`), or the `Average` of the two. These points can be specified by setting values in the `Timeframe (ms)` area: values not in the frame are automatically discarded (such as pacing artefacts in the beginning of a frame).

Special modes for resting potential definition are also available:

* `Voltage clamp`: in these measurements Phase 0 may be disturbed by stimulation artifacts, leading to falsely high `AMP` and `dVmax` values. This option is intended to combat these artefacts by only looking at the end `RP` and finding `AMP` during the actual overshoot period (the part above 0 mV) of the action potential.

* `Slow response`: this mode includes specific segmentation of continous measurements as well as a set of specific variables for slow response-type APs

\* In slow response APs, the `MDP` value is taken as the `RP`

\*\* The `AMP` value is set not to be further than 10 ms from the time of the `dVmax` value, as `dVmax` occurs during Phase 0, and the `AMP` point is reached at the end of Phase 0.

\*\*\* These values are calculated from raw data, but the presence of the notch is detected by fitting a polynomial on the first third of the repolarization.

### 6. Plot creation and corrections ###

When a plot is created, a preview is shown in the main window with additional pop-up views available by their corresponding buttons. `Matplotlib`: an interactive window provided by the Matplotlib library. Saving as an image file and zooming are available. `Cursor`: the same window, but 0 mV for the y axis is the MDP of the first chosen file, and 0 ms for the x axis is the CT. Therefore, all values are relative to the action potential itself in this mode.

When corrections are used, every potential's MDP or AMP is matched to the respective variable of the first potential by default. If a row is selected in the table, then all potentials will be matched to the chosen potential. This enables comparison of changes independently from some technical issues (e. g., unintended changes in the impalement). But be aware that this might also mask important drug effects! Only use these functions when appropriate.

If multiple rows are selected in the table (by Ctrl + Click or Shift + Click), only those will be plotted.

### 7. Manual analysis ###

After selecting exactly one row in the table, click the `Manual analysis` button. A window containing textboxes and a `Matplotlib` window will show up. Values of the points under the cursor on the `x` (time) and `y` (voltage) axes can be read in the bottom right corner of the plot window. The following further options are available in this window:

- `Click`: the value of the selected point will be shown in the plot, absolute values are shown

- `Keyboard button press`: the value of the selected point will be considered as a reference point: mouse clicks will show relative values (distances) from this point

- `Mouse scroll`: scrolling the mouse wheel removes the reference point, clicking shows absolute values again

First, fill the `MDP`, `CT`, and `Max mV` values, corresponding to the mean diastolic potential or resting potential, the time from 0 ms to the upstroke of the potential, and the highest point of the potential at the end of Phase 0.

Then, click the `Calculate` button. The `AMP` value will be automatically calculated, as well as the voltages corresponding to APD90–10 (90–10% of repolarization), making it easier to look up the corresponding time values. The rest of the values need to be filled manually. When all values are filled, click the `Correct with CT` button, and the CT value will be added to all time variables.

Finally, click `Accept` and close the plot window. The next time you click `Start`, the manually analysed potential will show up in the originally selected row of table with an 'M' appended to its `ID`.

Notes: any values can be left blank, they will be replaced with `NA` in the table. Triangulation is calculated automatically, and derivatives (`dVmax`, `dVmin`, `NdVmin`) are also calculated automatically.

### 8. Averaging potentials ###

After processing some files, multiple lines can be selected from the table (by Ctrl + Click or Shift + Click). When the lines are selected, pressing the `Average` button at the bottom will average all of the selected potentials, and show a preview of the output in the main window. A pop-up will ask to either `Create` the averaged potential or preview it in `Matplotlib`. When the `Create` button is clicked, the new file is automatically added to the selected files pane and the original (unaveraged) file is removed. If a previous average is present from the same source file, it will be overwritten, leading to a single averaged file from each original file.

Automatic averaging (`AutoAverage`) is also possible. This function takes each file individually. First, it averages all of the potentials within the file, then finds any potentials that are different from the overall average, and removes them from the final average. Depending on the quality of the recordings, this should be a quick way to get noise-free averages.

### 9. Exporting data ###

The data comprising created tables and figures can all be exported by clicking on the `Export` button at the bottom.

The table and the data for recreating the figures are exported as CSV files to the current location.

The figures themselves can be saved from the `Matplotlib` pop-up window.

### 10. Resetting ###

The `Reset` button clears all changes made to the interface. Files remain unchanged.

### 11. ID database ###

This feature is optional. When no database is provided or no match is found in it, the name of the file will be used. A sample database, `AP-database.csv` is provided with the sample files for reference.

The database should be formatted as follows:

`Date, number, info`

`210815VM, 14, Ventricular muscle`

`210817PF, 2, Purkinje fiber`

The first column contains the date of the measurement, the second column contains the ordinal number of the file so that each recording can have a separate number, and the third column contains any additional information about the recording. ID's generated based on the names of the files are generated and matched against ID's generated from the database. The filenames are separated at a `.` or a `_` character, any alphanumeric character before this delimiter forms the `Date` portion, and any number after it forms the `Number` portion of the ID. If there is no database provided or no match is found, the ID's and filenames are displayed in the tables and plots. If there is a match, the ID and the matching additional info will be shown.

A few examples of ID generation:

210412PF_134.csv becomes `210412PF.134`, where the `Date` component is `210412PF` and the `File` number is `134`.

120321ABCDE.00003.tsv becomes `120321ABCDE.3`, where the `Date` component is `120321ABCDE` and the `File` number is `3`.

By selecting the second tab of the appliaction screen (`ID database creation`), you can also create your own ID database. If no database file is specified, a new one is created, but it is suggested to create this file once, and keep adding to it to build up a reliable database. When opening the `ID database creation` tab, a sample fillout is shown, you can remove it by pressing `Reset`. If you want to keep some entries from being reset, click the tickboxes in their corresponding frames.

Before starting to create your database, make sure that the names of your files are correctly understood in ActionPytential by looking at the `ID` column in the table when working with your files.


# Credit #

GUI is provided by [PySimpleGUI](https://pypi.org/project/PySimpleGUI/).

Plots are generated using [Matplotlib](https://matplotlib.org/).

Color generation is based on [this](https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/) post by Martin Ankerl.


# Disclaimer #

This software is provided as is under the GPLv3 license, without any warranties. No user-related data is logged or processed. The current screen resolution is checked at each run for optimal window sizing, but this data is not stored either.

Bug reports and feautre requests are encouraged via the provided e-mail address, but pull requests are not accepted at this time.


# Contact #

You can reach the developer of this project at: thomas.hastings@tutanota.com