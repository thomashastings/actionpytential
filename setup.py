import setuptools  

__version__="0.8.1"

def readme():
    try:
        with open('README.md') as f:
            return f.read()
    except IOError:
        return ''

dependency_list=[]

try:
    import PySimpleGUI
except ImportError:
    dependency_list.append("pysimplegui")

try:
    import matplotlib
except ImportError:
    dependency_list.append("matplotlib")

try:
    import numpy
except ImportError:
    dependency_list.append("numpy")

try:
    import scipy
except ImportError:
    dependency_list.append("scipy")

try:
    import yaml
except ImportError:
    dependency_list.append("pyyaml")

setuptools.setup(
    name="ActionPytential",
    version=__version__,
    author="Thomas Hastings",
    author_email="thomas.hastings@tutanota.com",
    url="https://bitbucket.org/thomashastings/actionpytential/src/master/",
    description="A tool for analyzing and visualizing cardiac action potentials recorded by any technique.",
    long_description=readme(),
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Operating System :: OS Independent"
    ),
    install_requires=dependency_list
)
